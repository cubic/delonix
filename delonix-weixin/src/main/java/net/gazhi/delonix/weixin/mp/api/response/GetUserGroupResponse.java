package net.gazhi.delonix.weixin.mp.api.response;

import com.google.gson.annotations.SerializedName;

public class GetUserGroupResponse extends BaseResponse {

	@SerializedName("groupid")
	int groupId;

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

}
