package net.gazhi.delonix.weixin.mp;

import java.util.LinkedList;
import java.util.List;

public final class MPConfig {

	private String appId;

	private String appSecret;

	private String token;

	private final List<String> cabackIPList = new LinkedList<String>();

	// 暂不支持加密方式
	private String encodingAESKey;

	private String accessToken;

	public MPConfig() {
	}

	public MPConfig(String appId, String appSecret, String token) {
		this.appId = appId;
		this.appSecret = appSecret;
		this.token = token;
	}

	public String getAppId() {
		return appId;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public String getToken() {
		return token;
	}

	public String getEncodingAESKey() {
		return encodingAESKey;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public List<String> getCabackIPList() {
		return cabackIPList;
	}

}
