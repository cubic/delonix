package net.gazhi.delonix.weixin.mp.api.response;

import com.google.gson.annotations.SerializedName;

public class GetTokenResponse extends BaseResponse {

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("expires_in")
	private Integer expiresIn;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}
}
