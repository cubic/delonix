package net.gazhi.delonix.weixin.mp.api.response;

import com.google.gson.annotations.SerializedName;

public class GetCallbackIPResponse extends BaseResponse {

	@SerializedName("ip_list")
	private String[] ipList;

	public String[] getIpList() {
		return ipList;
	}

	public void setIpList(String[] ipList) {
		this.ipList = ipList;
	}

}
