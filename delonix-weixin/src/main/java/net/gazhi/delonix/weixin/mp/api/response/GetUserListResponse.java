package net.gazhi.delonix.weixin.mp.api.response;

import com.google.gson.annotations.SerializedName;

public class GetUserListResponse extends BaseResponse {

	private int total;
	private int count;
	@SerializedName("next_openid")
	private String nextOpenId;
	private Data data;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getNextOpenId() {
		return nextOpenId;
	}

	public void setNextOpenId(String nextOpenId) {
		this.nextOpenId = nextOpenId;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	/**
	 * 对应返回的 JSON 结构
	 * 
	 * @author Jeffrey Lin
	 *
	 */
	public static class Data {

		@SerializedName("openid")
		private String[] openIds;

		public String[] getOpenIds() {
			return openIds;
		}

		public void setOpenIds(String[] openIds) {
			this.openIds = openIds;
		}

	}

}
