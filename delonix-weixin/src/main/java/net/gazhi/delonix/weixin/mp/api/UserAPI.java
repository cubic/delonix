package net.gazhi.delonix.weixin.mp.api;

import java.util.HashMap;
import java.util.Map;

import net.gazhi.delonix.weixin.mp.MPConfig;
import net.gazhi.delonix.weixin.mp.api.response.BaseResponse;
import net.gazhi.delonix.weixin.mp.api.response.CreateGroupResponse;
import net.gazhi.delonix.weixin.mp.api.response.GetGroupsResponse;
import net.gazhi.delonix.weixin.mp.api.response.GetUserGroupResponse;
import net.gazhi.delonix.weixin.mp.api.response.GetUserInfoResponse;
import net.gazhi.delonix.weixin.mp.api.response.GetUserListResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 用户管理API
 * 
 * @author Jeffrey Lin
 *
 */
public class UserAPI {

	private static Log log = LogFactory.getLog(UserAPI.class);

	/**
	 * 获取用户列表
	 * 
	 * @param config
	 * @param nextOpenId
	 * @return
	 */
	public static GetUserListResponse getUserList(MPConfig config, String nextOpenId) {
		String url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=" + config.getAccessToken() + "&next_openid=";
		if (nextOpenId != null && nextOpenId.length() > 0) {
			url += nextOpenId;
		}
		GetUserListResponse response = APIUtils.executeSSLGet(url, GetUserListResponse.class);
		if (response != null && response.getErrcode() != 0) {
			log.error("Failed to get user list: " + response.getErrcode() + " " + response.getErrmsg());
			return null;
		}
		return response;
	}

	/**
	 * 获取用户详细信息
	 * 
	 * @param config
	 * @param openId
	 * @return
	 */
	public static GetUserInfoResponse getUserInfo(MPConfig config, String openId) {
		String url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + config.getAccessToken() + "&openid=" + openId + "&lang=zh_CN";
		GetUserInfoResponse response = APIUtils.executeSSLGet(url, GetUserInfoResponse.class);
		if (response != null && response.getErrcode() != 0) {
			log.error("Failed to get user info: " + response.getErrcode() + " " + response.getErrmsg());
			return null;
		}
		return response;
	}

	/**
	 * 设置用户备注名
	 * 
	 * @param config
	 * @param openId
	 * @param remark
	 * @return
	 */
	public static boolean updateUserRemark(MPConfig config, String openId, String remark) {
		String url = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=" + config.getAccessToken();
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openid", openId);
		param.put("remark", remark);
		BaseResponse response = APIUtils.executeSSLPost(url, param, BaseResponse.class);
		return response != null && response.getErrcode() == 0;
	}

	/**
	 * 创建分组
	 * 
	 * @param config
	 * @param name
	 * @return
	 */
	public static CreateGroupResponse createGroup(MPConfig config, String name) {
		String url = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=" + config.getAccessToken();
		Map<String, Object> param = new HashMap<String, Object>();
		Map<String, Object> group = new HashMap<String, Object>();
		param.put("group", group);
		group.put("name", name);
		CreateGroupResponse response = APIUtils.executeSSLPost(url, param, CreateGroupResponse.class);
		if (response != null && response.getErrcode() == 0) {
			return response;
		}
		return null;
	}

	/**
	 * 查询所有分组
	 * 
	 * @param config
	 * @return
	 */
	public static GetGroupsResponse getGroups(MPConfig config) {
		String url = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=" + config.getAccessToken();
		GetGroupsResponse response = APIUtils.executeSSLGet(url, GetGroupsResponse.class);
		if (response != null && response.getErrcode() == 0) {
			return response;
		}
		return null;
	}

	/**
	 * 查询用户所在分组
	 * 
	 * @param config
	 * @param openId
	 * @return
	 */
	public static GetUserGroupResponse getUserGroup(MPConfig config, String openId) {
		String url = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=" + config.getAccessToken();
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openid", openId);
		GetUserGroupResponse response = APIUtils.executeSSLPost(url, param, GetUserGroupResponse.class);
		if (response != null && response.getErrcode() == 0) {
			return response;
		}
		return null;
	}

	/**
	 * 修改分组名
	 * 
	 * @param config
	 * @param id
	 * @param name
	 * @return
	 */
	public static boolean updateGroup(MPConfig config, int id, String name) {
		String url = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=" + config.getAccessToken();
		Map<String, Object> param = new HashMap<String, Object>();
		Map<String, Object> group = new HashMap<String, Object>();
		group.put("id", id);
		group.put("name", name);
		param.put("group", group);
		BaseResponse response = APIUtils.executeSSLPost(url, param, BaseResponse.class);
		if (response.getErrcode() == 0) {
			return true;
		}
		log.error("Failed to update group: " + response.getErrcode() + ", " + response.getErrmsg());
		return false;
	}

	/**
	 * 移动用户分组
	 * 
	 * @param config
	 * @param openId
	 * @param toGroupId
	 * @return
	 */
	public static boolean updateMemberGroup(MPConfig config, String openId, int toGroupId) {
		String url = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=" + config.getAccessToken();
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openid", openId);
		param.put("to_groupid", toGroupId);
		BaseResponse response = APIUtils.executeSSLPost(url, param, BaseResponse.class);
		if (response.getErrcode() == 0) {
			return true;
		}
		log.error("Failed to update member group: " + response.getErrcode() + ", " + response.getErrmsg());
		return false;
	}

	public static boolean batchUpdateMembersGroup(MPConfig config, String[] openIds, int toGroupId) {
		String url = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=" + config.getAccessToken();
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openid_list", openIds);
		param.put("to_groupid", toGroupId);
		BaseResponse response = APIUtils.executeSSLPost(url, param, BaseResponse.class);
		if (response.getErrcode() == 0) {
			return true;
		}
		log.error("Failed to batch update members group: " + response.getErrcode() + ", " + response.getErrmsg());
		return false;
	}
}
