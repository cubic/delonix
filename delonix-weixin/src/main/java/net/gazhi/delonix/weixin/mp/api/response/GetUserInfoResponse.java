package net.gazhi.delonix.weixin.mp.api.response;

import java.util.Date;

import net.gazhi.delonix.weixin.mp.gson.DateInSecondsAdapter;
import net.gazhi.delonix.weixin.mp.gson.NumericalBooleanAdapter;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

public class GetUserInfoResponse extends BaseResponse {

	@JsonAdapter(NumericalBooleanAdapter.class)
	private boolean subscribe;

	@JsonAdapter(DateInSecondsAdapter.class)
	@SerializedName("subscribe_time")
	private Date subscribeTime;

	@SerializedName("openid")
	private String openId;

	@SerializedName("unionid")
	private String unionId;

	@SerializedName("nickname")
	private String nickName;

	private int sex;

	private String language;

	private String city;

	private String province;

	private String country;

	@SerializedName("headimgurl")
	private String headImgUrl;

	private String remark;

	public boolean getSubscribe() {
		return subscribe;
	}

	public boolean isSubscribe() {
		return subscribe;
	}

	public void setSubscribe(boolean subscribe) {
		this.subscribe = subscribe;
	}

	public Date getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(Date subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUnionId() {
		return unionId;
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
