<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../../../inc/taglib.jsp"%>
<json:array var="d" items="${ dirs }">
	<json:object>
		<json:property name="id" value="${ d.id }" />
		<json:property name="pId" value="${ d.parent.id }" />
		<json:property name="name" value="${ d.name }" />
		<json:property name="isParent" value="${ 1 }" />
		<json:property name="open" value="${ 1 }" />
	</json:object>
</json:array>
