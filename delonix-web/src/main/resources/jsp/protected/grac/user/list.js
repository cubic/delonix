// <%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>页面脚本 \\

// <%// 此行有秘密哦：%>权限树的数据<%="\n"%>var dirId=${form.dirId>0?form.dirId:0};<%="\n"%>var zNodes = <%@ include file="list_zNodes.jsp" %>;

var setting = {
	view : {
		addHoverDom : addHoverDom,
		removeHoverDom : removeHoverDom,
		selectedMulti : false
	},
	data : {
		simpleData : {
			enable : true
		}
	},
	callback : {
		onClick : function(event, treeId, treeNode, clickFlag) {
			$("#dirId").val(treeNode.id);
			if ($("#chkbox-immediately").val() == "1") {
				$("#btn-submit").click();
			}
		}
	}
};

// 显示自定义按钮
function addHoverDom(treeId, treeNode) {
	var aObj = $("#" + treeNode.tId + "_a");
	if (treeNode.editNameFlag || $("#btn_add_" + treeNode.tId).size() > 0) {
		return;
	}
	// “添加”按钮
	aObj.append('<span class="button add" id="btn_add_' + treeNode.tId + '" onfocus="this.blur();"></span>');
	$("#btn_add_" + treeNode.tId).click(function(event) {
		window.open("./edit?dirId="+treeNode.id, "_self");
		return false;
	});
}

// 隐藏自定义按钮
function removeHoverDom(treeId, treeNode) {
	$("#btn_add_" + treeNode.tId).unbind().remove();
}

$(document).ready(function() {
	$("#table-thead").fixedThead();

	$(document).on("click.bs.button.data-api", "#state-btn-group", function(e) {
		var $btn = $(e.target);
		if (!$btn.is(".btn")) {
			$btn = $btn.parents(".btn");
		}
		var sid = $btn.prop("href");
		var $input = $(sid.substr(sid.indexOf("#")));
		$input.val($btn.is(".active") ? 1 : 0);
	});

	var tree = $.fn.zTree.init($("#user-dir-ztree"), setting, zNodes);
	if (dirId) {
		var node = tree.getNodeByParam("id", dirId, null);
		tree.selectNode(node);
	}

});