<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../../../inc/taglib.jsp"%>

<c:set var="HTML_HEAD_APPENDENT">
	<script><%@ include file="list.js" %></script>
	<style>
#state-btn-group .fa-check-square-o, #state-btn-group .active .fa-square-o
	{
	display: none;
}

#state-btn-group .fa-square-o, #state-btn-group .active .fa-check-square-o
	{
	display: inline;
}

#tree-container .well.well-sm {
	min-height: 600px;
	margin-right: 1px;
}

@media ( max-width : 767px) {
	#tree-container .well.well-sm {
		min-height: 0;
		margin-bottom: 0;
		margin-right: 0;
	}
}

@media ( min-width : 768px) {
	#tree-container.collapse {
		display: block;
		visibility: visible;
	}
}
</style>
</c:set>

<c:set var="HTML_PAGE_NAVBAR_CONTENT">
	<p class="navbar-text">${ dxfn:lang("系统管理 &gt;&gt; 用户 &gt;&gt; 列表", "System &gt;&gt; User &gt;&gt; List") }</p>
	<form class="navbar-form navbar-right" role="search">
		<input type="hidden" name="dirId" id="dirId" value="${ form.dirId }">
		<input type="hidden" name="listSubdir" id="chkbox-listSubdir" value="${ form.listSubdir?1:0 }">
		<input type="hidden" name="immediately" id="chkbox-immediately" value="${ form.immediately?1:0 }">

		<div class="input-group">
			<input type="text" class="trim form-control" placeholder="Search Users" name="keyword" value="${ form.keyword }">
			<div class="input-group-btn">
				<button class="btn btn-success" type="submit" id="btn-submit">
					<span class="glyphicon glyphicon-search"></span>
				</button>
			</div>
		</div>
	</form>
</c:set>

<c:set var="HTML_MAIN">
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-5 collapse" id="tree-container">
			<div class="row">
				<div class="well well-sm">
					<div class="btn-group" data-toggle="buttons" id="state-btn-group">
						<a href="#chkbox-listSubdir" class="btn btn-default btn-sm ${ form.listSubdir?'active':'' }" style="min-width: 130px;"><i class="fa fa-check-square-o"></i><i class="fa fa-square-o"></i> ${ dxfn:lang("显示下级用户", "List Subdir Users") }
						</a> <a href="#chkbox-immediately" class="btn btn-default btn-sm ${ form.immediately?'active':'' }" style="min-width: 130px;"><i class="fa fa-check-square-o"></i><i class="fa fa-square-o"></i>
							${ dxfn:lang("即时导航模式", "List Immediately") } </a>
					</div>
					<ul id="user-dir-ztree" class="ztree"></ul>
				</div>
			</div>
		</div>
		<div class="col-lg-9 col-md-8 col-sm-7">
			<div class="row">
				<table class="table table-bordered table-striped table-hover table-condensed">
					<thead id="table-thead">
						<tr class="center">
							<th><a href="#tree-container" class="visible-xs-block" data-toggle="collapse"><i class="fa fa-sitemap"></i></a></th>
							<th class="${ dxfn:lang('', 'hidden-xs') }">${ dxfn:lang("中文名", "Chinese Name") }</th>
							<th class="${ dxfn:lang('hidden-xs', '') }">${ dxfn:lang("英文名", "English Name") }</th>
							<th>${ dxfn:lang("排序编号", "Order Code") }</th>
							<th>${ dxfn:lang("状态", "State") }</th>
							<th class="left hidden-xs hidden-sm">${ dxfn:lang("目录", "Directory") }</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ users }" var="u" varStatus="vs">
							<tr class="center">
								<th class="hcol">${ vs.count }</th>
								<td class="${ dxfn:lang('', 'hidden-xs') }"><a href="show?id=${ u.id }">${ u.nameZh }</a></td>
								<td class="${ dxfn:lang('hidden-xs', '') }"><a href="show?id=${ u.id }">${ u.nameEn }</a></td>
								<td>${ u.showOrder }</td>
								<td><i class="fa ${ u.isDisabled ? 'fa-ban' : 'fa-check-circle' }" title="${ u.isDisabled ? dxfn:lang('停用', 'Disabled') : dxfn:lang('启用', 'Actived') }"></i></td>
								<td class="left hidden-xs hidden-sm">${ u.userDir.fullName }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<nav>${ pageInfo.htmlNav }</nav>
		</div>
	</div>

</c:set>

<%@ include file="../../../inc/layout.jsp"%>