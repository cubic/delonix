// <%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>页面脚本 \\

$(function() {

	/***************************************************************************
	 * 表单事件：提交前，禁用按钮，并显示 spin 动画
	 **************************************************************************/
	function beforeSubmit() {
		universal.modalLoading("${dxfn:lang('正在保存用户……', 'Saving User...')}");
	}

	/***************************************************************************
	 * 表单事件：处理结果
	 **************************************************************************/
	function success(responseText, statusText) {
		// 停1秒再显示
		window.setTimeout(function() {
			if (responseText.trim() == "") {
				universal.modalLoading("${dxfn:lang('用户已保存，正在跳转……', 'User Saved, Redirecting...')}");
				// 停1秒再跳转
				window.setTimeout(function() {
					window.open("./list", "_self");
				}, 1000);
			} else {
				universal.modalLoading(false);
				universal.modalWarning({
					title : "${dxfn:lang('保存失败', 'Faild to Save')}",
					msg : responseText
				});
			}
		}, 1000);
	}

	/***************************************************************************
	 * 页面加载，初始化
	 **************************************************************************/
	$(document).ready(function() {

		// 切换状态控件
		$("#state-btn-group").radioBtnGroup("${ user.isDisabled ? '1' : '0' }");
		
		$("#locale-btn-group").radioBtnGroup("${ user.locale=='en' ? 'en' : 'zh' }");
		

		// 验证表单，成功后通过 ajax 提交
		$('#role-form').validator({
			timely : true,
			focusCleanup : true,
			theme : 'yellow_bottom',
			valid : function(form) {
				$(form).ajaxSubmit({
					beforeSubmit : beforeSubmit,
					success : success
				});
			}
		});

	});

});