<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../../../inc/taglib.jsp"%>

<c:set var="HTML_HEAD_APPENDENT">
	<script><%@ include file="edit.js" %></script>
	<style>
#role-btn-group .fa-check-square, #role-btn-group .active .fa-square-o
	{
	display: none;
}

#role-btn-group .fa-square-o, #role-btn-group .active .fa-check-square
	{
	display: inline;
}
</style>
</c:set>

<c:set var="HTML_PAGE_NAVBAR_CONTENT">
	<p class="navbar-text">${ dxfn:lang("系统管理 &gt;&gt; 用户 &gt;&gt; ", "System &gt;&gt; User &gt;&gt; ") }${ dxfn:lang( user.id>0?"修改":"新增", user.id>0?"Modify":"Add") }</p>
	<a class="btn btn-primary btn-xs navbar-btn" href="./list"><i class="fa fa-reply"></i> ${ dxfn:lang("返回列表", "Back To List") }</a>
</c:set>

<c:set var="HTML_MAIN">
	<br>
	
	roleIds
	<form role="form" method="post" id="user-form" action="save" class="col-sm-6 col-md-4">
		<input type="hidden" name="id" value="${ user.id }">
		<div class="form-group">
			<label>所在目录 User Directory:</label>
			<div class="text-muted">${ dir.fullName }</div>
		</div>
		<div class="form-group">
			<label>登录名 Login Name:</label>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-user"></i>
				</div>
				<input type="text" class="trim form-control" id="loginName" name="loginName" value="${ user.loginName }" data-rule="${ dxfn:lang('登录名', 'Login-Name') }:username">
			</div>
		</div>
		<div class="form-group">
			<label>密码 Password:</label>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-lock"></i>
				</div>
				<input type="password" class="trim form-control" id="loginPsw" name="loginPsw" data-rule="${ dxfn:lang('密码', 'Password') }:${ user.id>0 ? '' : 'required;' }length[6~32]">
			</div>
		</div>
		<div class="form-group">
			<label>确认密码 Confirm Password:</label>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-lock"></i>
				</div>
				<input type="password" class="trim form-control" id="confirmPsw" data-rule="${ dxfn:lang('确认密码', 'Confirm-Password') }:match[eq, loginPsw]">
			</div>
		</div>
		<div class="form-group">
			<label>中文名 Chiniese Name:</label>
			<div class="input-group">
				<div class="input-group-addon">中</div>
				<input type="text" class="trim form-control" id="nameZh" name="nameZh" value="${ user.nameZh }" data-rule="${ dxfn:lang('中文名', 'Chiniese-Name') }:required;length[2~80]">
			</div>
		</div>
		<div class="form-group">
			<label>英文名 English Name:</label>
			<div class="input-group">
				<div class="input-group-addon">En</div>
				<input type="text" class="trim form-control" id="nameEn" name="nameEn" value="${ user.nameEn }" data-rule="${ dxfn:lang('英文名', 'English-Name') }:required;length[2~80]">
			</div>
		</div>
		<div class="form-group">
			<label>排序编号 Order Code:</label>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-sort-alpha-asc"></i>
				</div>
				<input type="text" class="trim form-control" id="showOrder" name="showOrder" value="${ user.showOrder }" data-rule="${ dxfn:lang('排序编号', 'Order-Code') }:required;length[1~80]">
			</div>
		</div>

		<div class="form-group">
			<label>邮箱 Email:</label>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-envelope"></i>
				</div>
				<input type="text" class="trim form-control" id="email" name="email" value="${ user.email }" data-rule="${ dxfn:lang('邮箱', 'Email') }:email">
			</div>
		</div>

		<div class="form-group">
			<label>备注 Remark:</label>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-file-text-o"></i>
				</div>
				<input type="text" class="trim form-control" id="remark" name="remark" value="${ user.remark }">
			</div>
		</div>

		<div class="form-group">
			<label>用户状态 User State:</label>
			<div class="btn-group" data-toggle="buttons" id="state-btn-group">
				<div class="btn btn-default btn-sm">
					<input type="radio" name="isDisabled" value="0"><i class="fa fa-square-o"></i> ${ dxfn:lang('启用', 'Actived') }
				</div>
				<div class="btn btn-default btn-sm">
					<input type="radio" name="isDisabled" value="1"><i class="fa fa-square-o"></i> ${ dxfn:lang('停用', 'Disabled') }
				</div>
			</div>
		</div>

		<div class="form-group">
			<label>界面语言 Language:</label>
			<div class="btn-group" data-toggle="buttons" id="locale-btn-group">
				<div class="btn btn-default btn-sm">
					<input type="radio" name="locale" value="zh"><i class="fa fa-square-o"></i> ${ dxfn:lang('中文', 'Chiniese') }
				</div>
				<div class="btn btn-default btn-sm">
					<input type="radio" name="locale" value="en"><i class="fa fa-square-o"></i> ${ dxfn:lang('英文', 'English') }
				</div>
			</div>
		</div>

		<div class="form-group">
			<label>授予角色 Grant Roles:</label>
			<div id="role-btn-group" class="btn-group-vertical" role="group" data-toggle="buttons" style="display: block;">
				<c:forEach items="${ roles }" var="r">
					<label class="btn btn-default btn-sm" style="text-align: left;"><i class="fa fa-check-square"></i><i class="fa fa-square-o"></i> <input type="checkbox"> ${ r.name } </label>
				</c:forEach>
			</div>
		</div>

		<div style="height: 15px;"></div>
		<button type="submit" class="btn btn-primary pull-right">
			<i class="fa fa-save"></i> 保存 Save
		</button>
		<a class="btn btn-info" href="./list"><i class="fa fa-reply"></i> 取消 Cancel</a>
		<div class="clearfix">
			<br> <br>
		</div>

	</form>

</c:set>

<%@ include file="../../../inc/layout.jsp"%>