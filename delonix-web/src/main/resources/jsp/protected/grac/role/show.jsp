<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../../../inc/taglib.jsp"%>

<c:set var="HTML_HEAD_APPENDENT">
	<script><%@ include file="show.js" %></script>
</c:set>

<c:set var="HTML_PAGE_NAVBAR_CONTENT">
	<p class="navbar-text">${ dxfn:lang("系统管理 &gt;&gt; 角色 &gt;&gt; 详情", "System &gt;&gt; Role &gt;&gt; Detail") }</p>
	<a class="btn btn-primary btn-xs navbar-btn" href="./list"><i class="fa fa-reply"></i> ${ dxfn:lang("返回列表", "Back To List") }</a>
</c:set>

<c:set var="HTML_PAGE_TOOLBAR_CONTENT">
	<c:if test="${ role.id >1 }">
		<c:if test="${ dxfn:isAuthorized('/grac/role/delete') }">
			<a id="btn-delete" class="btn btn-default btn-xs" href="./delete?id=${ role.id }"><i class="glyphicon glyphicon-remove"></i> ${ dxfn:lang("删除", "Delete") }</a>
		</c:if>
		<c:if test="${ dxfn:isAuthorized('/grac/role/edit') }">
			<a class="btn btn-default btn-xs" href="./edit?id=${ role.id }"><i class="glyphicon glyphicon-pencil"></i> ${ dxfn:lang("修改", "Modify") }</a>
		</c:if>
	</c:if>
</c:set>

<c:set var="HTML_MAIN">
	<br>
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-6">
			<fieldset>
				<legend>${ dxfn:lang("角色基本信息：", "Base Role Info:") }</legend>
				<div class="well well-sm ">
					<table class="detail">
						<tr>
							<th>${ dxfn:lang("中 文 名：", "Name(zh):") }</th>
							<td>${ role.nameZh }</td>
						</tr>
						<tr>
							<th>${ dxfn:lang("英 文 名：", "Name(en):") }</th>
							<td>${ role.nameEn }</td>
						</tr>
						<tr>
							<th>${ dxfn:lang("排序编号：", "Order Code:") }</th>
							<td>${ role.showOrder }</td>
						</tr>
						<tr>
							<th>${ dxfn:lang("角色状态：", "Role State:") }</th>
							<td><i class="fa ${ role.isDisabled ? 'fa-ban' : 'fa-check-circle' }"></i> ${ role.isDisabled ? dxfn:lang('停用', 'Disabled') : dxfn:lang('启用', 'Actived') }</td>
						</tr>
					</table>
				</div>
			</fieldset>
		</div>
		<div class="col-lg-3 col-md-4 col-sm-6">
			<fieldset>
				<legend>${ dxfn:lang("授权用户：", "Granted Users:") }</legend>
				<div class="well well-sm ">
					<c:forEach var="u" items="${ role.users }">
						<div>
							<i class="fa fa-user${ u.isDisabled ? '-times' : '' }"></i>${ u.isDisabled ? '' : ' ' }${ u.name }
						</div>
					</c:forEach>
				</div>
			</fieldset>
		</div>

		<div class="col-lg-3 col-md-4 col-sm-6">
			<fieldset>
				<legend>${ dxfn:lang("访问控制：", "Access Control:") }</legend>
				<c:forEach items="${ modules }" var="mdl">
					<div class="well well-sm ">
						<ul class="ztree" id="ztree-${ mdl.id }"></ul>
					</div>
				</c:forEach>
			</fieldset>
		</div>
	</div>

	<div class="clearfix">
		<br> <br>
	</div>

</c:set>

<%@ include file="../../../inc/layout.jsp"%>