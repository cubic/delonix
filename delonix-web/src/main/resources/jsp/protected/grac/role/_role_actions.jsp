<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../../../inc/taglib.jsp"%>
<json:array items="${ modules }" var="mdl">
	<json:array>
		<c:set var="checkMdl" value="${ false }" />
		<c:forEach var="ctrl" items="${ mdl.controllers }">
			<c:set var="checkCtrl" value="${ false }" />
			<c:forEach var="act" items="${ ctrl.actions }">
				<json:object>
					<json:property name="id" value="${ act.id }" />
					<json:property name="pId" value="C${ ctrl.id }" />
					<json:property name="name" value="${ act.name }" />
					<c:if test="${ dxfn:contains(role.actions, act) }">
						<json:property name="checked" value="${ 1 }" />
						<c:set var="checkCtrl" value="${ true }" />
					</c:if>
				</json:object>
			</c:forEach>
			<json:object>
				<json:property name="id" value="C${ ctrl.id }" />
				<json:property name="pId" value="M${ mdl.id }" />
				<json:property name="name" value="${ ctrl.name }" />
				<json:property name="isParent" value="${ 1 }" />
				<json:property name="open" value="${ 1 }" />
				<c:if test="${ checkCtrl }">
					<json:property name="checked" value="${ 1 }" />
					<c:set var="checkMdl" value="${ true }" />
				</c:if>
			</json:object>
		</c:forEach>
		<json:object>
			<json:property name="id" value="M${ mdl.id }" />
			<json:property name="name" value="${ mdl.name }" />
			<json:property name="isParent" value="${ 1 }" />
			<json:property name="open" value="${ 1 }" />
			<c:if test="${ checkMdl }">
				<json:property name="checked" value="${ 1 }" />
			</c:if>
		</json:object>
	</json:array>
</json:array>
