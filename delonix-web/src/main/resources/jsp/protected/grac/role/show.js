// <%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>页面脚本 \\

// <%// 此行有秘密：%>权限树的数据<%="\n"%>var actionData = <%@ include file="_role_actions.jsp" %>;

$(function() {

	var trees = [];
	var treeSetting = {
		check : {
			enable : true,
			chkboxType : {
				"Y" : "ps",
				"N" : "ps"
			}
		},
		data : {
			keep : {
				parent : true
			},
			simpleData : {
				enable : true
			}
		},
		callback : {
			beforeCheck : function(treeId, treeNode) {
				return false;
			}
		}
	};

	// 删除后跳转
	function redirectAfterDeleted() {
		var url = "./list";
		window.open(url, "_self");
	}

	// 页面加载，初始化
	$(document).ready(function() {
		$(".ztree").each(function(i) {
			trees[trees.length] = $.fn.zTree.init($(this), treeSetting, actionData[i]);
		});

		$("#btn-delete").click(function() {
			var url = $(this).attr("href");
			universal.modalConfirm({
				msg : "${dxfn:lang('请确认是否删除角色？', 'Please confirm whether to delete or not?')}",
				callback : function() {
					$.get(url, function() {
						universal.modalSuccess({
							msg : "角色已删除 Role Deleted!"
						});
						window.setTimeout(redirectAfterDeleted, 1000);
					});
				}
			});

			return false;
		});
	});

});