<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../../../inc/taglib.jsp"%>

<c:set var="HTML_HEAD_APPENDENT">
	<script><%@ include file="edit.js" %></script>
</c:set>

<c:set var="HTML_PAGE_NAVBAR_CONTENT">
	<p class="navbar-text">${ dxfn:lang("系统管理 &gt;&gt; 角色 &gt;&gt; ", "System &gt;&gt; Role &gt;&gt; ") }${ dxfn:lang( role.id>0?"修改":"新增", role.id>0?"Modify":"Add") }</p>
	<a class="btn btn-primary btn-xs navbar-btn" href="./list"><i class="fa fa-reply"></i> ${ dxfn:lang("返回列表", "Back To List") }</a>
</c:set>

<c:set var="HTML_MAIN">
	<br>
	<form role="form" method="post" id="role-form" action="save" class="col-sm-6 col-md-4">
		<input type="hidden" name="id" value="${ role.id }"><input type="hidden" name="actionIds" id="actionIds">
		<div class="form-group">
			<label>中文名 Chiniese Name:</label>
			<div class="input-group">
				<div class="input-group-addon">中</div>
				<input type="text" class="trim form-control" id="nameZh" name="nameZh" value="${ role.nameZh }" data-rule="${ dxfn:lang('中文名', 'Chiniese-Name') }:required;length[2~80]">
			</div>
		</div>
		<div class="form-group">
			<label>英文名 English Name:</label>
			<div class="input-group">
				<div class="input-group-addon">En</div>
				<input type="text" class="trim form-control" id="nameEn" name="nameEn" value="${ role.nameEn }" data-rule="${ dxfn:lang('英文名', 'English-Name') }:required;length[2~80]">
			</div>
		</div>
		<div class="form-group">
			<label>排序编号 Order Code:</label>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-sort-alpha-asc"></i>
				</div>
				<input type="text" class="trim form-control" id="showOrder" name="showOrder" value="${ role.showOrder }" data-rule="${ dxfn:lang('排序编号', 'Order-Code') }:required;length[1~80]">
			</div>
		</div>

		<div class="form-group">
			<label>角色状态 Role State:</label>
			<div class="btn-group" data-toggle="buttons" id="state-btn-group">
				<div class="btn btn-default btn-sm">
					<input type="radio" name="isDisabled" value="1"><i class="fa fa-square-o"></i> ${ dxfn:lang('停用', 'Disabled') }
				</div>
				<div class="btn btn-default btn-sm">
					<input type="radio" name="isDisabled" value="0"><i class="fa fa-square-o"></i> ${ dxfn:lang('启用', 'Actived') }
				</div>
			</div>
		</div>

		<div class="form-group">
			<label>访问控制 Access Control:</label>
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-cubes"></i>
				</div>
				<%
					// 模块选择下拉列表
				%>
				<div class="dropdown" id="switch-tree-dropdown">
					<div class="list-group dropdown-menu" role="menu" style="padding: 0; min-width: 208px">
						<c:forEach items="${ modules }" var="mdl">
							<a class="list-group-item list-group-item-warning" href="#tree-tab-${ mdl.id }" data-toggle="tab"><i class="fa fa-cube"></i> ${ mdl.name }</a>
						</c:forEach>
					</div>
				</div>
				<button id="btn-switch-tree-dropdown" type="button" class="btn btn-default active form-control" data-toggle="dropdown" data-target="#switch-tree-dropdown">${ dxfn:lang("请选择模块", "Please Select a Module") }</button>
				<div class="input-group-btn">
					<button type="button" class="btn btn-info" id="btn-switch-all-tree">
						<i class="fa fa-sitemap"></i>
					</button>
				</div>
			</div>
			<%
				// 功能模块树控件
			%>
			<div class="tab-content" id="tree-tab-content">
				<c:forEach items="${ modules }" var="mdl">
					<div class="well well-sm tab-pane fade" style="margin-bottom: 1px;" id="tree-tab-${ mdl.id }">
						<ul class="ztree" id="ztree-${ mdl.id }"></ul>
					</div>
				</c:forEach>
			</div>
		</div>

		<div style="height: 15px;"></div>
		<button type="submit" class="btn btn-primary pull-right">
			<i class="fa fa-save"></i> 保存 Save
		</button>
		<a class="btn btn-info" href="./list"><i class="fa fa-reply"></i> 取消 Cancel</a>
		<div class="clearfix">
			<br> <br>
		</div>
	</form>

</c:set>

<%@ include file="../../../inc/layout.jsp"%>