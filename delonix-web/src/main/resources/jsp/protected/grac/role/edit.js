// <%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>页面脚本 \\

// <%// 此行有秘密：%>权限树的数据<%="\n"%>var actionData = <%@ include file="_role_actions.jsp" %>;

$(function() {

	var trees = [];
	var treeSetting = {
		check : {
			enable : true,
			chkboxType : {
				"Y" : "ps",
				"N" : "ps"
			}
		},
		data : {
			keep : {
				parent : true
			},
			simpleData : {
				enable : true
			}
		},
		callback : {
			// 单击勾选
			onClick : function(event, treeId, treeNode) {
				$.fn.zTree.getZTreeObj(treeId).checkNode(treeNode, !treeNode.checked, true, true);
			}
		}
	};

	/***************************************************************************
	 * 表单事件：组装表单数据之前，设置 actionIds 的值
	 **************************************************************************/
	function beforeSerialize() {
		var nodeIds = new Array();
		for (var i = 0; i < trees.length; i++) {
			var nodes = trees[i].getCheckedNodes(true);
			for (var j = 0; j < nodes.length; j++) {
				var n = nodes[j];
				if (!n.isParent) {
					nodeIds[nodeIds.length] = n.id;
				}
			}
		}
		$("#actionIds").val(nodeIds);
		$("#actionIds").attr("disabled", $("#actionIds").val() ? false : true);
	}

	/***************************************************************************
	 * 表单事件：提交前，禁用按钮，并显示 spin 动画
	 **************************************************************************/
	function beforeSubmit() {
		universal.modalLoading("${dxfn:lang('正在保存角色……', 'Saving Role...')}");
	}

	/***************************************************************************
	 * 表单事件：处理结果
	 **************************************************************************/
	function success(responseText, statusText) {
		// 停1秒再显示
		window.setTimeout(function() {
			if (responseText.trim() == "") {
				universal.modalLoading("${dxfn:lang('角色已保存，正在跳转……', 'Role Saved, Redirecting...')}");
				// 停1秒再跳转
				window.setTimeout(function() {
					window.open("./list", "_self");
				}, 1000);
			} else {
				universal.modalLoading(false);
				universal.modalWarning({
					title : "${dxfn:lang('保存失败', 'Faild to Save')}",
					msg : responseText
				});
			}
		}, 1000);
	}

	/***************************************************************************
	 * 页面加载，初始化
	 **************************************************************************/
	$(document).ready(function() {

		// Action 模块树
		$(".ztree").each(function(i) {
			trees[trees.length] = $.fn.zTree.init($(this), treeSetting, actionData[i]);
		});

		// 切换角色状态控件
		$("#state-btn-group").radioBtnGroup("${ role.isDisabled ? '1' : '0' }");

		// 模块下拉列表项，把模块名字设置到按钮上
		$('#switch-tree-dropdown a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
			$("#btn-switch-tree-dropdown").html($(this).html());
		});

		// 显示隐藏所有模块
		$("#btn-switch-all-tree").click(function() {
			$("#tree-tab-content .tab-pane").removeClass("active").removeClass("in");
			var btn = $(this);
			if (btn.is(".active")) {
				btn.removeClass("active");
				$("#btn-switch-tree-dropdown").html("${ dxfn:lang('请选择模块', 'Please Select a Module') }");
			} else {
				btn.addClass("active");
				$("#tree-tab-content .tab-pane").addClass("active").addClass("in");
				$("#btn-switch-tree-dropdown").html("${ dxfn:lang('所有模块', 'All Modules') }");
			}
		});

		// 验证表单，成功后通过 ajax 提交
		$('#role-form').validator({
			timely : true,
			focusCleanup : true,
			theme : 'yellow_bottom',
			valid : function(form) {
				$(form).ajaxSubmit({
					beforeSerialize : beforeSerialize,
					beforeSubmit : beforeSubmit,
					success : success
				});
			}
		});

	});

});