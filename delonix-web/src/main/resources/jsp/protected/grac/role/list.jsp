<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../../../inc/taglib.jsp"%>

<c:set var="HTML_HEAD_APPENDENT">
	<script>
		$(function() {
			$("#role-table-thead").fixedThead();
		});
	</script>
</c:set>

<c:set var="HTML_PAGE_NAVBAR_CONTENT">
	<p class="navbar-text">${ dxfn:lang("系统管理 &gt;&gt; 角色 &gt;&gt; 列表", "System &gt;&gt; Role &gt;&gt; List") }</p>
	<c:if test="${ dxfn:isAuthorized('/grac/role/edit') }">
		<a class="btn btn-primary btn-xs navbar-btn" href="./edit"><i class="glyphicon glyphicon-plus"></i> ${ dxfn:lang("新增角色", "Add Role") }</a>
	</c:if>
	<form class="navbar-form navbar-right" role="search" action="./list">
		${ pageInfo.htmlInput }
		<div class="input-group">
			<input type="text" class="trim form-control" placeholder="Search Roles" name="keyword" value="${ param.keyword }">
			<div class="input-group-btn">
				<button class="btn btn-success" type="submit">
					<span class="glyphicon glyphicon-search"></span>
				</button>
			</div>
		</div>
	</form>
</c:set>

<c:set var="HTML_MAIN">
	<div class="row">
		<table class="table table-bordered table-striped table-hover table-condensed">
			<thead id="role-table-thead">
				<tr class="center">
					<th>&nbsp;</th>
					<th class="${ dxfn:lang('', 'hidden-xs') }">${ dxfn:lang("中文名", "Chinese Name") }</th>
					<th class="${ dxfn:lang('hidden-xs', '') }">${ dxfn:lang("英文名", "English Name") }</th>
					<th>${ dxfn:lang("排序编号", "Order Code") }</th>
					<th>${ dxfn:lang("状态", "State") }</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${ roles }" var="r" varStatus="vs">
					<tr class="center">
						<th class="hcol">${ vs.count }</th>
						<td class="${ dxfn:lang('', 'hidden-xs') }"><a href="show?id=${ r.id }">${ r.nameZh }</a></td>
						<td class="${ dxfn:lang('hidden-xs', '') }"><a href="show?id=${ r.id }">${ r.nameEn }</a></td>
						<td>${ r.showOrder }</td>
						<td><i class="fa ${ r.isDisabled ? 'fa-ban' : 'fa-check-circle' }" title="${ r.isDisabled ? dxfn:lang('停用', 'Disabled') : dxfn:lang('启用', 'Actived') }"></i></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<nav>${ pageInfo.htmlNav }</nav>
</c:set>

<%@ include file="../../../inc/layout.jsp"%>