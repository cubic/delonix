// <%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>页面脚本 \\

// <%// 此行有秘密哦：%>权限树的数据<%="\n"%>var zNodes = <%@ include file="home_zNodes.jsp" %>;

function getNode(id) {
	for (var i = 0; i < zNodes.length; i++) {
		var n = zNodes[i];
		if (n.id == id) {
			return n;
		}
	}
	return false;
}

var setting = {
	view : {
		addHoverDom : addHoverDom,
		removeHoverDom : removeHoverDom,
		selectedMulti : false
	},
	edit : {
		enable : true,
		showRemoveBtn : false,
		showRenameBtn : false,
		drag : {
			isCopy : false,
			isMove : true
		}
	},
	data : {
		keep : {
			parent : true
		},
		simpleData : {
			enable : true
		}
	},
	callback : {
		onDrop : moveDir
	}
};

// 显示自定义按钮
function addHoverDom(treeId, treeNode) {
	var aObj = $("#" + treeNode.tId + "_a");
	if (treeNode.editNameFlag || $("#btn_add_" + treeNode.tId).size() > 0) {
		return;
	}
	// “添加”按钮
	aObj.append('<span class="button add" id="btn_add_' + treeNode.tId + '" onfocus="this.blur();"></span>');
	$("#btn_add_" + treeNode.tId).click(function() {
		edit(treeNode.id);
		return false;
	});
	// 根目录，不能修改/删除
	if (treeNode.pId == null) {
		return;
	}
	// “修改”按钮
	aObj.append('<span class="button edit" id="btn_edit_' + treeNode.tId + '" onfocus="this.blur();"></span>');
	$("#btn_edit_" + treeNode.tId).click(function() {
		edit(treeNode.pId, treeNode.id);
		return false;
	});
	// “删除”按钮
	aObj.append('<span class="button remove" id="btn_del_' + treeNode.tId + '" onfocus="this.blur();"></span>');
	$("#btn_del_" + treeNode.tId).click(function() {
		deleteDir(treeNode);
		return false;
	});
}

// 隐藏自定义按钮
function removeHoverDom(treeId, treeNode) {
	$("#btn_add_" + treeNode.tId).unbind().remove();
	$("#btn_edit_" + treeNode.tId).unbind().remove();
	$("#btn_del_" + treeNode.tId).unbind().remove();
}

// 显示编辑表单
function edit(parentId, dirId) {
	$("#dir-form").clearForm();
	var p = getNode(parentId);
	$("#parentId").val(p.id);
	$("#parentName").html(p.fullName);
	if (dirId) {
		var n = getNode(dirId);
		$("#dirId").val(n.id);
		$("#nameZh").val(n.nameZh);
		$("#nameEn").val(n.nameEn);
	}
	var modal = $("#edit-modal");
	$(".modal-title", modal).html(dirId ? "${dxfn:lang('修改目录', 'Modify Directory')}" : "${dxfn:lang('新增子目录', 'Add Child Directory')}");
	modal.modal("show");
}

// 删除目录
function deleteDir(treeNode) {
	var url = "delete?id=" + treeNode.id;
	universal.modalConfirm({
		msg : "${dxfn:lang('请确认是否删除目录？', 'Please confirm whether to delete the directory or not?')}<br><br>" + treeNode.fullName,
		callback : function() {
			universal.modalLoading("${dxfn:lang('正在删除目录……','Deleting Directory...')}");
			window.setTimeout(function() {
				$.get(url, afterDelete);
			}, 500)
		}
	});
}

// 处理删除结果
function afterDelete(responseText) {
	universal.modalLoading(false);
	if (responseText.trim() == "") {
		universal.modalSuccess({
			msg : "${dxfn:lang('目录已删除！','Directory Deleted!')}"
		});
		universal.reload(1000);
	} else {
		universal.modalWarning({
			title : "${dxfn:lang('删除失败！', 'Faild to Deleted!')}",
			msg : responseText
		});
	}
}

// 移动
function moveDir(event, treeId, treeNodes, targetNode, moveType) {
	var id = treeNodes[0].id;
	var targetId = targetNode.id;
	var url = "move?id=" + id + "&targetId=" + targetId + "&moveType=" + moveType;
	universal.modalLoading("${dxfn:lang('正在移动目录……','Moving Directory...')}");
	window.setTimeout(function() {
		$.get(url, afterMove);
	}, 500);
};
// 处理移动结果
function afterMove(responseText) {
	universal.modalLoading(false);
	if (responseText.trim() == "") {
		universal.modalSuccess({
			msg : "${dxfn:lang('目录移动成功！','Directory Moved!')}"
		});
	} else {
		universal.modalWarning({
			title : "${dxfn:lang('移动目录失败！', 'Faild to Move!')}",
			msg : responseText
		});
	}
	universal.reload(2000);
}

// 处理保存结果
function afterSubmit(responseText, statusText) {
	// 停500秒再显示结果，交互效果好一点
	window.setTimeout(function() {
		universal.modalLoading(false);
		if (responseText.trim() == "") {
			universal.modalSuccess({
				msg : "${dxfn:lang('保存成功！','Directory Saved!')}"
			});
			universal.reload(1000);
		} else {
			universal.modalWarning({
				title : "${dxfn:lang('保存失败！', 'Faild to Save!')}",
				msg : responseText
			});
		}
	}, 500);
}

// Document Ready 初始化
var tree = false;
$(document).ready(function() {
	tree = $.fn.zTree.init($("#tree"), setting, zNodes);

	// 验证编辑表单，成功后通过 ajax 提交
	$('#dir-form').validator({
		timely : true,
		focusCleanup : true,
		theme : 'yellow_bottom',
		valid : function(form) {
			$(form).ajaxSubmit({
				// 提交前，并显示 spin 动画
				beforeSubmit : function() {
					universal.modalLoading("${dxfn:lang('正在保存目录……','Saving Directory...')}");
				},
				success : afterSubmit
			});
		}
	});
});
