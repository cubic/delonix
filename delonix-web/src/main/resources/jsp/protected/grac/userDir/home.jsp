<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../../../inc/taglib.jsp"%>

<c:set var="HTML_HEAD_APPENDENT">
	<script><%@ include file="home.js" %></script>
</c:set>

<c:set var="HTML_PAGE_NAVBAR_CONTENT">
	<p class="navbar-text">${ dxfn:lang("系统管理 &gt;&gt; 用户目录", "System &gt;&gt; User Directory") }</p>
</c:set>

<c:set var="HTML_MAIN">
	<ul id="tree" class="ztree"></ul>

	<div class="modal" id="edit-modal">
		<form role="form" method="post" id="dir-form" action="save">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<div class="modal-body">

						<input type="hidden" name="id" id="dirId" value=""><input type="hidden" name="parentId" id="parentId">
						<div class="form-group">
							<label>上级目录 Parent Directory:</label>
							<p id="parentName" class="form-control-static">中</p>
						</div>
						<div class="form-group">
							<label>中文名 Chiniese Name:</label>
							<div class="input-group">
								<div class="input-group-addon">中</div>
								<input type="text" class="trim form-control" id="nameZh" name="nameZh" data-rule="中文名:required;length[2~80]">
							</div>
						</div>
						<div class="form-group">
							<label>英文名 English Name:</label>
							<div class="input-group">
								<div class="input-group-addon">En</div>
								<input type="text" class="trim form-control" id="nameEn" name="nameEn" data-rule="英文名:required;length[2~80]">
							</div>
						</div>

						<div class="clearfix"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-info" data-dismiss="modal">取消 Cancel</button>
						<button type="submit" class="btn btn-primary" id="btn-save-dir">保存 Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</c:set>

<%@ include file="../../../inc/layout.jsp"%>