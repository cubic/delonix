// <%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>页面脚本
$(function() {

	// 登录成功，跳转
	function redirectAfterLogin() {
		var url = "./";
		window.open(url, "_self");
	}

	// 刷新验证码
	function reloadRandom() {
		$("#img-random").attr("src", "loginRandom?" + new Date().getTime());
		$("#random-code").val("");
	}

	// 提交前，禁用按钮，并显示 spin 动画
	function beforeSubmit(str, $form) {
		universal.modalLoading("正在登录 Processing Login...");
	}

	// 处理登录结果
	function afterSubmit(responseText, statusText) {
		// 停1000秒再显示结果，交互效果好一点
		window.setTimeout(function() {
			if (responseText.trim() == "") {
				universal.modalLoading("正在跳转 Redirecting...");
				window.setTimeout(redirectAfterLogin, 1000);
			} else {
				universal.modalLoading(false);
				// 显示登录失败结果，并刷新验证码
				universal.modalWarning({
					title : "登录失败 Failed to Login",
					msg : responseText
				});
				window.setTimeout(reloadRandom, 1000);
			}
		}, 1000);
	}

	function initLogin() {

		// 验证码右边的刷新按钮
		$("#btn-reload-random").click(function() {
			reloadRandom();
			$(this).blur();
		});

		// 密码框获得焦点时，自动清空
		$("#login-psw").focus(function() {
			this.value = "";
		});

		// 验证表单，成功后通过 ajax 提交
		$('#login-form').validator({
			timely : true,
			focusCleanup : true,
			theme : 'yellow_bottom',
			valid : function(form) {
				$(form).ajaxSubmit({
					beforeSubmit : beforeSubmit,
					success : afterSubmit
				});
			}
		});
	}

	// 页面加载，初始化
	$(document).ready(initLogin);

});
