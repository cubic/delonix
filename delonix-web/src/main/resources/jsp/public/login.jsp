<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../inc/taglib.jsp"%>

<c:set var="HTML_HEAD_APPENDENT">
	<script><%@ include file="login.js" %></script>
</c:set>

<c:set var="HTML_MAIN">
	<br>
	<div class="row">
		<div class="col-xs-0 col-sm-3 col-md-3 col-lg-4"></div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">用户登录 User Login</h3>
				</div>
				<div class="panel-body">

					<form role="form" method="post" id="login-form" action="login">
						<div class="form-group">
							<label for="loginName">登录名 Login Name:</label>
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-user"></span>
								</div>
								<input type="text" class="trim form-control" id="login-name" name="loginName" value="${ param.loginName }" data-rule="${ dxfn:lang('登录名', 'Login Name') }:required">
							</div>
						</div>
						<div class="form-group">
							<label for="loginPsw">密码 Password:</label>
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-lock"></span>
								</div>
								<input type="password" class="trim form-control" id="login-psw" name="loginPsw" value="${ param.loginPsw }" data-rule="${ dxfn:lang('密码', 'Password') }:required">
							</div>
						</div>
						<div class="form-group">
							<label for="randomCode">验证码 Validation Code:</label>
							<div class="input-group">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-eye-open"></span>
								</div>
								<input type="text" class="trim form-control" id="random-code" name="randomCode" maxlength="4" data-rule="${ dxfn:lang('验证码', 'Validation Code') }:required;length[4]">
								<div class="input-group-addon" style="padding: 2px">
									<img src="loginRandom" id="img-random">
								</div>
								<div class="input-group-btn">
									<button class="btn " type="button" id="btn-reload-random">
										<span class="glyphicon glyphicon-refresh"></span>
									</button>
								</div>
							</div>
						</div>
						<div style="height: 15px;"></div>
						<button type="submit" class="btn btn-info btn-block">
							<i class="fa fa-sign-in"></i> &nbsp;<b>登录 Login</b>
						</button>
					</form>

				</div>
				<div class="panel-footer">
					<fmt:message key="app.html-title" />
				</div>
			</div>

		</div>
		<div class="col-xs-0 col-sm-3 col-md-3 col-lg-4"></div>
	</div>
</c:set>

<%@ include file="../inc/layout.jsp"%>