<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setAttribute("forward_request_uri", request.getAttribute("javax.servlet.forward.request_uri"));
%>

<c:if test="${ isAjax }">
	<div>path = ${ forward_request_uri }</div>
	<br>
	<div>数据权限不足！</div>
	<div>Data Access Denied!</div>
	<br>
</c:if>

<c:if test="${ !isAjax }">
	<c:set var="HTML_MAIN">
		<br>
		<div class="alert alert-danger">
			<div>path = ${ forward_request_uri }</div>
			<br>
			<div>
				数据权限不足，转到： <a href="${pageContext.request.contextPath}/" target="_top">主页</a>。
			</div>
			<div>
				Data Access Denied, Goto: <a href="${pageContext.request.contextPath}/" target="_top">Home Page</a>.
			</div>
		</div>
	</c:set>
	<c:set var="LAYOUT_HIDE_NAVBAR" value="${ true }" />
	<%@ include file="../../inc/layout.jsp"%>
</c:if>
