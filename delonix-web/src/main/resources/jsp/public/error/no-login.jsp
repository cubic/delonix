<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${ isAjax }">
	<br>会话超时，请重新 <a href="${pageContext.request.contextPath}/login" target="_top">登录</a>。<br>
	<br>Session Timeout, Please <a href="${pageContext.request.contextPath}/login" target="_top">Login</a> Again.<br>
	<br>
</c:if>

<c:if test="${ !isAjax }">
	<c:set var="HTML_HEAD_APPENDENT">
		<script>
			window.setTimeout(function() {
				window.open("${pageContext.request.contextPath}/login", "_top")
			}, 2000);
		</script>
	</c:set>
	<c:set var="HTML_MAIN">
		<br>
		<div class="alert alert-danger">
			<br>未登录或会话超时，请先 <a href="${pageContext.request.contextPath}/login" target="_top">登录</a>。<br> <br>No Login or Session Timeout, Please <a
				href="${pageContext.request.contextPath}/login" target="_top">Login</a> First.
		</div>
	</c:set>
	<c:set var="LAYOUT_HIDE_NAVBAR" value="${ true }" />
	<%@ include file="../../inc/layout.jsp"%>
</c:if>