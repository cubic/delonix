<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true" isErrorPage="true"%>
<%@ include file="../../inc/taglib.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />
<%
	request.setAttribute("forward_request_uri", request.getAttribute("javax.servlet.forward.request_uri"));
%>

<c:if test="${ isAjax }">
	<div>出错啦</div>
	<div>Error Happend</div>
	<br>
	<div>错误详情 Error Details:</div>
	<pre class="bg-danger">
path=${ forward_request_uri }; user=${ dxfn:loginUser().loginName }; time=<fmt:formatDate value="${now}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
		<br>${pageContext.errorData.throwable}
	</pre>
</c:if>

<c:if test="${ !isAjax }">
	<c:set var="HTML_MAIN">
		<br>
		<div class="alert alert-danger">
			<div>
				出错啦，转到： <a href="${pageContext.request.contextPath}/" target="_top">主页</a>
			</div>
			<div>
				Error Happend, goto: <a href="${pageContext.request.contextPath}/" target="_top">Home Page</a>
			</div>
			<br>
			<div>错误详情 Error Details:</div>
			<pre class="bg-warning">
path=${ forward_request_uri }; user=${ dxfn:loginUser().loginName }; time=<fmt:formatDate value="${now}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
					<br>${pageContext.errorData.throwable}
</pre>
		</div>
	</c:set>
	<c:set var="LAYOUT_HIDE_NAVBAR" value="${ true }" />
	<%@ include file="../../inc/layout.jsp"%>
</c:if>