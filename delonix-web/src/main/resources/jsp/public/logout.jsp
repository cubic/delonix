<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../inc/taglib.jsp"%>

<c:set var="HTML_HEAD_APPENDENT">
	<script>
		window.setTimeout(function() {
			window.open("login", "_self");
		}, 1000);
	</script>
</c:set>

<c:set var="HTML_MAIN">
	<br>
	<div class="alert alert-success">已退出 Logouted.</div>
</c:set>


<c:set var="LAYOUT_HIDE_NAVBAR" value="${ true }" />
<%@ include file="../inc/layout.jsp"%>