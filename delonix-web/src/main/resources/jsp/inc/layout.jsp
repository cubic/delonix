<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ include file="../inc/taglib.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8" />
<base target="_self" />
<meta name="robots" content="noindex,nofollow" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title><fmt:message key="app.html-title" /></title>
<%
	// jQuery 及相关组件
%>
<script src="${pageContext.request.contextPath}/static/components/jquery/jquery-2.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/static/components/jquery/jquery.form-3.51.0.min.js"></script>
<%
	// zTree 组件
%>
<link href="${ pageContext.request.contextPath}/static/components/zTree-3.5.17/css/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/static/components/zTree-3.5.17/jquery.ztree.min.js"></script>
<%
	// bootstrap 及相关组件
%>
<script src="${pageContext.request.contextPath}/static/components/bootstrap-3.3.0/bootstrap.min.js"></script>
<link href="${ pageContext.request.contextPath}/static/components/bootstrap-3.3.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${ pageContext.request.contextPath}/static/components/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<%
	// niceValidator 表单验证插件
%>
<link href="${ pageContext.request.contextPath}/static/components/validator-0.7.3/jquery.validator.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/static/components/validator-0.7.3/jquery.validator.js"></script>
<script src="${pageContext.request.contextPath}/static/components/validator-0.7.3/local/${ dxfn:lang('zh', 'en') }.js"></script>
<%
	// 所有页面全局脚本
%>
<script src="${pageContext.request.contextPath}/static/universal.js?${static_last_modified}&context=${pageContext.request.contextPath}"></script>
<%
	// 自定义样式表
%>
<link href="${ pageContext.request.contextPath}/static/css/style.css?${static_last_modified}" rel="stylesheet" type="text/css" />
<%
	// 具体页面补充脚本或样式
%>
<c:out value="${ HTML_HEAD_APPENDENT }" escapeXml="false" />
<c:out value="
" escapeXml=" false" />
</head>
<body>
	<c:if test="${ !LAYOUT_HIDE_NAVBAR }">
		<header class="navbar navbar-inverse navbar-static-top" role="banner">
			<div class="container">
				<div class="navbar-header">
					<c:if test="${ !dxfn:isGuest() }">
						<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
						</button>
					</c:if>
					<a class="navbar-brand" href="${pageContext.request.contextPath}/"><fmt:message key="app.navbar-brand-text" /></a>
				</div>
				<c:if test="${ !dxfn:isGuest() }">
					<div class="navbar-collapse collapse" role="navigation">
						<ul class="nav navbar-nav">
							<c:forEach var="it" items="${ dxfn:authorizedRootMenuItems('MAIN') }">
								<li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">${ it.name }<b class="caret"></b></a>
									<ul class="dropdown-menu dropdown-menu-global">
										<c:forEach var="chit" items="${ it.childItems }">
											<li><a href="${pageContext.request.contextPath}${ chit.href }">${ chit.name }</a></li>
										</c:forEach>
									</ul></li>
							</c:forEach>
						</ul>
						<div class="navbar-text navbar-right">
							<i class="fa fa-user"></i> ${ dxfn:loginUser().name }&nbsp;<a href="${pageContext.request.contextPath}/logout" class="btn-xs btn-success"><i class="fa fa-sign-out"></i> ${ dxfn:lang("退出", "Logout") }</a>
							<a href="#" onclick="universal.switchLocale(this);return false;" class="btn-xs btn-success" title="${ dxfn:lang('Switch language to English.', '切换语言为中文') }">${ dxfn:lang('中', 'En') }&nbsp;<i
								class="fa fa-fighter-jet"></i> ${ dxfn:lang('En', '中') }
							</a>
						</div>
					</div>
				</c:if>
			</div>
		</header>
	</c:if>

	<c:if test="${ fn:length(HTML_PAGE_NAVBAR_CONTENT)>0 }">
		<nav class="navbar navbar-default page-navbar" role="navigation">
			<div class="container">
				<div class="row">
					<p class="navbar-text navbar-text-home">
						<a href="${pageContext.request.contextPath}/"><i class="glyphicon glyphicon-home"></i></a>
					</p>
					<c:out value="${ HTML_PAGE_NAVBAR_CONTENT }" escapeXml="false" />
				</div>
			</div>
		</nav>
	</c:if>

	<c:if test="${ fn:length(HTML_PAGE_TOOLBAR_CONTENT)>0 }">
		<nav class="navbar navbar-default page-toolbar" role="navigation">
			<div class="container">
				<c:out value="${ HTML_PAGE_TOOLBAR_CONTENT }" escapeXml="false" />
			</div>
		</nav>
	</c:if>



	<c:if test="${ LAYOUT_HIDE_MAIN_CONTAINER }">
		<c:out value="${ HTML_MAIN }" escapeXml="false" />
	</c:if>

	<c:if test="${ !LAYOUT_HIDE_MAIN_CONTAINER }">
		<div class="container">
			<c:out value="${ HTML_MAIN }" escapeXml="false" />
		</div>
	</c:if>

</body>
</html>