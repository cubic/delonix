<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="dxfn" uri="http://delonix.gazhi.net/jsp/tlib/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
	// 利用 delonix-mvn-plugin 插件，Maven 编译时会替换 static_last_modified 的值
	// 具体请参见：net.gazhi.delonix.mvn.SetStaticLastModifiedPropertyMojo
%>
<c:set var="static_last_modified" value="${static_last_modified}" />