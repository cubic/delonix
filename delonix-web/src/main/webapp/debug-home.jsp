<%@ page pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8" />
<base target="_self" />
<meta name="robots" content="noindex,nofollow" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Delonix Web Framework</title>
<link href="${ pageContext.request.contextPath}/static/components/bootstrap-3.3.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-xs-0 col-md-2 col-lg-3"></div>
			<div class="col-xs-12 col-md-8 col-lg-6">

				<div class="panel panel-primary">

					<div class="panel-heading">
						<h3 class="panel-title">感谢您的关注</h3>
					</div>

					<div class="panel-body">
						<p>Delonix 汇集了作者工作与学习中碰到的问题经过梳理后整理出的一些较为通用的方案，可作为一个快速开发的框架使用，欢迎探讨！</p>
						<p>不想显示本页面，您可以：</p>
						<ol>
							<li><p>
									覆盖 /index.jsp，只保留一行代码：&lt;jsp:forward page="index" /&gt;<br>然后添加处理 /index 这个请求的 Controller（推荐）
								</p></li>
							<li><p>覆盖 /index.jsp 为您想要的内容，或者其他您可以想到的方式</p></li>
						</ol>

						<div>
							<p>
								现在进入：<a href="login?loginName=sa&loginPsw=abc123">登录页面</a> | <a href="grac/user/list">用户管理</a>
							</p>
							<p>
								或者执行：<a href="init">数据初始化</a>
							</p>
							<p>
								代码托管：<a target="_blank" href="http://git.oschina.net/jlin/delonix">http://git.oschina.net/jlin/delonix</a>
							</p>
						</div>
					</div>

					<div class="panel-footer">
						<div class="pull-left">
							<img src="static/img/delonix-20x20.jpg" style="vertical-align: bottom;"> <a href="./">Delonix Web Framework</a>
						</div>
						<div class="pull-right">
							<a href="http://my.oschina.net/delonix" target="_blank">@JeffreyLin</a>
						</div>
						<div class="clearfix"></div>
					</div>

				</div>

			</div>
		</div>
	</div>
</body>
</html>