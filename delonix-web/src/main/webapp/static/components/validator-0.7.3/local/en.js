/*******************************************************************************
 * Themes, rules, and i18n support Locale: English
 ******************************************************************************/
(function(factory) {
	if (typeof define === 'function') {
		define(function(require, exports, module) {
			var $ = require('jquery');
			$._VALIDATOR_URI = module.uri;
			require('../src/jquery.validator')($);
			factory($);
		});
	} else {
		factory(jQuery);
	}
}(function($) {
	/*
	 * Global configuration
	 */
	$.validator.config({
		// stopOnError: false,
		// theme: 'yellow_right',
		defaultMsg : "This field is not valid.",
		loadingMsg : "Validating...",

		// Custom rules
		rules : {
			digits : [ /^\d+$/, "Accepts digits only." ],
			letters : [ /^[a-z]+$/i, "Accepts standard english characters only." ],
			tel : [ /^(?:(?:0\d{2,3}[\- ]?[1-9]\d{6,7})|(?:[48]00[\- ]?[1-9]\d{6}))$/, "Invalid tel number format." ],
			mobile : [ /^1[3-9]\d{9}$/, "Invalid mobile number format." ],
			email : [ /^[\w\+\-]+(\.[\w\+\-]+)*@[a-z\d\-]+(\.[a-z\d\-]+)*\.([a-z]{2,4})$/i, "Invalid email address format." ],
			ID_card : [ /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[A-Z])$/, "Invalid ID number format." ],
			url : [ /^(https?|ftp):\/\/[^\s]+$/i, "Invalid url format." ],
			username : [ /^\w{2,32}$/, "Accepts 2 to 32 numbers, letters and underlines only." ],
			accept : function(element, params) {
				if (!params)
					return true;
				var ext = params[0];
				return (ext === '*') || (new RegExp(".(?:" + (ext || "png|jpg|jpeg|gif") + ")$", "i")).test(element.value) || this.renderMsg("只接受{1}后缀", ext.replace('|', ','));
			}

		}
	});

	/*
	 * Default error messages
	 */
	$.validator.config({
		messages : {
			required : "{0} is required.",
			remote : "Please try another name.",
			integer : {
				'*' : "Please enter an integer.",
				'+' : "Please enter a positive integer.",
				'+0' : "Please enter a positive integer or 0.",
				'-' : "Please enter a negative integer.",
				'-0' : "Please enter a negative integer or 0."
			},
			match : {
				eq : "{0} must be equal to {1}.",
				neq : "{0} must be not equal to {1}.",
				lt : "{0} must be less than {1}.",
				gt : "{0} must be greater than {1}.",
				lte : "{0} must be less than or equal to {1}.",
				gte : "{0} must be greater than or equal to {1}."
			},
			range : {
				rg : "Please enter a number between {1} and {2}.",
				gte : "Please enter a number greater than or equal to {1}.",
				lte : "Please enter a number less than or equal to {1}."
			},
			checked : {
				eq : "Please check {1} items.",
				rg : "Please check between {1} and {2} items.",
				gte : "Please check at least {1} items.",
				lte : "Please check no more than {1} items."
			},
			length : {
				eq : "Please enter {1} characters.",
				rg : "Please enter a value between {1} and {2} characters long.",
				gte : "Please enter at least {1} characters.",
				lte : "Please enter no more than {1} characters.",
				eq_2 : "",
				rg_2 : "",
				gte_2 : "",
				lte_2 : ""
			}
		}
	});

	/*
	 * Themes
	 */
	var TPL_ARROW = '<span class="n-arrow"><b>◆</b><i>◆</i></span>';
	$.validator.setTheme({
		'simple_right' : {
			formClass : 'n-simple',
			msgClass : 'n-right'
		},
		'simple_bottom' : {
			formClass : 'n-simple',
			msgClass : 'n-bottom'
		},
		'yellow_top' : {
			formClass : 'n-yellow',
			msgClass : 'n-top',
			msgArrow : TPL_ARROW
		},
		'yellow_right' : {
			formClass : 'n-yellow',
			msgClass : 'n-right',
			msgArrow : TPL_ARROW
		},
		'yellow_right_effect' : {
			formClass : 'n-yellow',
			msgClass : 'n-right',
			msgArrow : TPL_ARROW,
			msgShow : function($msgbox, type) {
				var $el = $msgbox.children();
				if ($el.is(':animated'))
					return;
				if (type === 'error') {
					$el.css({
						left : '20px',
						opacity : 0
					}).delay(100).show().stop().animate({
						left : '-4px',
						opacity : 1
					}, 150).animate({
						left : '3px'
					}, 80).animate({
						left : 0
					}, 80);
				} else {
					$el.css({
						left : 0,
						opacity : 1
					}).fadeIn(200);
				}
			},
			msgHide : function($msgbox, type) {
				var $el = $msgbox.children();
				$el.stop().delay(100).show().animate({
					left : '20px',
					opacity : 0
				}, 300, function() {
					$msgbox.hide();
				});
			}
		}
	});
}));