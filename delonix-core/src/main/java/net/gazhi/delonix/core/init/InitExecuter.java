package net.gazhi.delonix.core.init;

import org.springframework.transaction.annotation.Transactional;

/**
 * 初始化调用器
 * 
 * @author Jeffrey Lin
 */
public class InitExecuter {

	private InitService[] initServices;

	public void setInitServices(InitService[] initServices) {
		this.initServices = initServices;
	}

	@Transactional
	public void execute() {
		if (initServices == null) {
			return;
		}
		for (InitService initService : initServices) {
			try {
				initService.init();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

}
