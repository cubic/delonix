package net.gazhi.delonix.core.service;

import java.sql.Timestamp;
import java.util.Calendar;

import net.gazhi.delonix.core.dao.Dao;
import net.gazhi.delonix.core.thread.ThreadContext;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 业务逻辑层 Service 基类
 * 
 * @author Jeffrey Lin
 * 
 */
public abstract class AbstractService {

	@Autowired
	protected Dao dao;

	public void setDao(Dao dao) {
		this.dao = dao;
	}

	/**
	 * 取得国际化消息
	 * 
	 * @param code
	 * @return
	 */
	protected String getMessage(String code) {
		return ThreadContext.getMessage(code);
	}

	/**
	 * 取得国际化消息(带参数)
	 * 
	 * @param code
	 * @param args
	 * @return
	 */
	protected String getMessage(String code, Object[] args) {
		return ThreadContext.getMessage(code, args);
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static Timestamp now() {
		return new Timestamp(Calendar.getInstance().getTimeInMillis());
	}

	protected Criteria createCriteria(Class<?> entityClass) {
		return dao.createCriteria(entityClass);
	}

	protected Criteria createCriteria(Class<?> entityClass, String alias) {
		return dao.createCriteria(entityClass, alias);
	}

}
