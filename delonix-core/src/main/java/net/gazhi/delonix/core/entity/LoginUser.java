package net.gazhi.delonix.core.entity;

import java.io.Serializable;
import java.util.Locale;

import javax.servlet.http.HttpSession;

/**
 * 登录用户信息
 * 
 * @author Jeffrey Lin
 * 
 */
public class LoginUser extends BilingualName implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String DELONIX_LOGIN_USER_SESSION_KEY = "DELONIX_LOGIN_USER_SESSION_KEY";

	/**
	 * 从 session 提取登录用户，未登录就返回 guset 用户
	 * 
	 * @param session
	 * @return
	 */
	public static final LoginUser getLoginUser(HttpSession session) {
		LoginUser loginUser = (LoginUser) session.getAttribute(DELONIX_LOGIN_USER_SESSION_KEY);
		if (loginUser == null) {
			loginUser = guest();
			setLoginUser(session, loginUser);
		}
		return loginUser;
	}

	/**
	 * 把登录用户保存到 session
	 * 
	 * @param session
	 * @param loginUser
	 */
	public static final void setLoginUser(HttpSession session, LoginUser loginUser) {
		session.setAttribute(DELONIX_LOGIN_USER_SESSION_KEY, loginUser);
	}

	public static final LoginUser guest() {
		return new LoginUser(-1L, "guest", "Guest", "访客", "en");
	}

	private long id;
	private String loginName;
	private String nameZh;
	private String nameEn;
	private Locale locale;

	public LoginUser(long id, String loginName, String nameEn, String nameZh, String locale) {
		this.id = id;
		this.loginName = loginName;
		this.nameEn = nameEn;
		this.nameZh = nameZh;
		this.setLocale(locale);
	}

	public long getId() {
		return id;
	}

	public String getLoginName() {
		return loginName;
	}

	@Override
	public String getNameZh() {
		return nameZh;
	}

	@Override
	public String getNameEn() {
		return nameEn;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = new Locale(locale);
	}
}
