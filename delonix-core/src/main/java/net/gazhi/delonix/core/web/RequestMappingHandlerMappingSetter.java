package net.gazhi.delonix.core.web;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * RequestMappingHandlerMapping 设置器<br>
 * 在 Spring MVC ServletContext 定义， 用于将 ServletContext 的
 * RequestMappingHandlerMapping<br>
 * 保存到 Root ApplicationContext 的 Holder里，供初始化和过滤器在进行权限判断时使用
 * 
 * @author Jeffrey Lin
 * 
 */
public class RequestMappingHandlerMappingSetter {

	@Autowired
	private RequestMappingHandlerMappingHolder holder;

	@Autowired
	private RequestMappingHandlerMapping mp;

	@PostConstruct
	public void setToParentContext() {
		holder.add(mp);
	}

	@PreDestroy
	public void removeFromParentContext() {
		holder.remove(mp);
		this.holder = null;
		this.mp = null;
	}

}
