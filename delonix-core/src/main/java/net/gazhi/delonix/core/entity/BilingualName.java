package net.gazhi.delonix.core.entity;

import java.util.Locale;

import net.gazhi.delonix.core.thread.ThreadContext;

/**
 * 中英文双语名称支持
 * 
 * @author Jeffrey Lin
 *
 */
public abstract class BilingualName {

	public abstract String getNameZh();

	public abstract String getNameEn();

	/**
	 * 根据当前用户使用的 Locale 返回中文或英文名字
	 * 
	 * @return
	 */
	public String getName() {
		return Locale.CHINESE.equals(ThreadContext.getLocale()) ? this.getNameZh() : this.getNameEn();
	}

}
