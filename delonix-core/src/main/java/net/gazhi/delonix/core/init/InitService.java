package net.gazhi.delonix.core.init;

/**
 * 初始化业务逻辑接口
 * 
 * @author Jeffrey Lin
 */
public interface InitService {

	public abstract void init() throws Exception;
}
