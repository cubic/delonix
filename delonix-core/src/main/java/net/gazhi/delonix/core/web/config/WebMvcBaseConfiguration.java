package net.gazhi.delonix.core.web.config;

import net.gazhi.delonix.core.validation.ExposeValueToArgsValidator;
import net.gazhi.delonix.core.web.UserLocaleResolver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.RequestToViewNameTranslator;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.view.DefaultRequestToViewNameTranslator;

/**
 * MVC 基本配置
 * 
 * @author Jeffrey Lin
 *
 */
@Configuration
public class WebMvcBaseConfiguration extends WebMvcConfigurationSupport {

	@Override
	public RequestMappingHandlerMapping requestMappingHandlerMapping() {
		RequestMappingHandlerMapping mapping = super.requestMappingHandlerMapping();
		mapping.setAlwaysUseFullPath(true);
		return mapping;
	}

	@Bean
	public RequestToViewNameTranslator viewNameTranslator() {
		DefaultRequestToViewNameTranslator translator = new DefaultRequestToViewNameTranslator();
		translator.setAlwaysUseFullPath(true);
		return translator;
	}

	@Bean
	public LocaleResolver localeResolver() {
		return new UserLocaleResolver();
	}

	/**
	 * 改用自定义的 ExposeValueToArgsValidator
	 */
	@Bean
	@Override
	protected Validator getValidator() {
		return new ExposeValueToArgsValidator();
	}

}
