package net.gazhi.delonix.core.dao;

import java.io.Serializable;
import java.util.Map;

import net.gazhi.delonix.core.entity.PrimitiveTypeMap;

import org.hibernate.transform.BasicTransformerAdapter;

/**
 * PrimitiveTypeMap 结果转换器<br>
 * 方便查询统计的时候组装统计结果的二次计算
 * 
 * @author Jeffrey Lin
 *
 */
public class AliasToPrimitiveTypeEntityMapResultTransformer extends BasicTransformerAdapter implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final AliasToPrimitiveTypeEntityMapResultTransformer INSTANCE = new AliasToPrimitiveTypeEntityMapResultTransformer();

	private AliasToPrimitiveTypeEntityMapResultTransformer() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object transformTuple(Object[] tuple, String[] aliases) {
		Map result = new PrimitiveTypeMap(tuple.length);
		for (int i = 0; i < tuple.length; i++) {
			String alias = aliases[i];
			if (alias != null) {
				result.put(alias, tuple[i]);
			}
		}
		return result;
	}

}
