package net.gazhi.delonix.core.dao;

/**
 * 分页信息类<br>
 * <br>
 * 
 * <b>分页查找：</b><br>
 * <br>
 * 第一步，创建对象，并设置当前页号和单页记录数<br>
 * PageInfo pageInfo = new PageInfo(pageNo, pageSize);<br>
 * <br>
 * 
 * 第二步，查找并设置总记录数<br>
 * int rsCount = select count(1) from ....<br>
 * pageInfo.setRsCount(rsCount);<br>
 * <br>
 * 
 * 第三步，limit查找<br>
 * query.setMaxResults(pageInfo.getPageSize());<br>
 * query.setFirstResult(pageInfo.getStartRsIndex());<br>
 * <br>
 * 
 * <b>页面调用：</b><br>
 * <br>
 * ① 输出保存页码的 input 和 js<br>
 * ${ pageInfo.htmlInput }<br>
 * <br>
 * 
 * ② 输出分页导航<br>
 * ${ pageInfo.htmlNav }
 * 
 * <pre>
 * 
 * @author Jeffrey Lin
 */
public class PageInfo {

	private static final String DEFAULT_JS_FUNCTION = "gotoPage";

	/** 表单控件的 id */
	private String inputId = "pageNo";

	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/** 表单控件的 name */
	private String inputName = "pageNo";

	public void setInputName(String inputName) {
		this.inputName = inputName;
	}

	/** 翻页 js 函数 */
	private String jsFunction = DEFAULT_JS_FUNCTION;

	public void setJsFunction(String jsFunction) {
		this.jsFunction = jsFunction;
	}

	/** 每页显示记录数 */
	private int pageSize = 20;

	/** 当前页码，从1开始，-1表示最后一页 */
	private int pageNo = 1;

	/** 总记录数，通过select count(1) from 计算获得 */
	public long rsCount = 0;

	public PageInfo() {
	}

	public PageInfo(int pageNo) {
		this.pageNo = pageNo == 0 ? 1 : pageNo;
	}

	public PageInfo(int pageNo, int pageSize) {
		this.pageNo = pageNo == 0 ? 1 : pageNo;
		if (pageSize > 0) {
			this.pageSize = pageSize;
		}
	}

	public int getPageSize() {
		return this.pageSize;
	}

	public long getRsCount() {
		return this.rsCount;
	}

	public int getPageNo() {
		return this.pageNo;
	}

	/** 获取总页数 */
	public int getPageCount() {
		int pageCount = 0;
		if (this.pageSize > 0 && this.rsCount >= 0) {
			pageCount = (int) (this.rsCount / this.pageSize + (this.rsCount % this.pageSize > 0 ? 1 : 0));
		}
		return pageCount;
	}

	/** 设置总记录数，通过select count(1) from 计算获得 */
	public void setRsCount(long rsCount) {
		if (rsCount > 0) {
			this.rsCount = rsCount;
		}
		int pageCount = this.getPageCount();
		if (this.pageNo < 0 || this.pageNo > pageCount) {
			this.pageNo = pageCount;
		}
	}

	/** 获取当前页第一条记录在所有记录中的索引 */
	public int getStartRsIndex() {
		if (this.rsCount >= 0 && this.pageNo > 0) {
			return (this.pageNo - 1) * this.pageSize;
		}
		return 0;
	}

	/**
	 * 输出 input 和翻页脚本
	 * 
	 * <pre>
	 * &lt;input id="pageNo" name="pageNo" type="hidden" value="1" />
	 * &lt;script>
	 * function gotoPage(pageNo){
	 * 	$('#pageNo').val(pageNo);
	 * 	$('#pageNo').parent('form').trigger('submit');
	 * }
	 * &lt;/script>
	 * </pre>
	 * 
	 * @return
	 */
	public String getHtmlInput() {
		StringBuilder str = new StringBuilder(200);
		str.append("<input id=\"").append(this.inputId).append("\" name=\"").append(this.inputName).append("\" type=\"hidden\" value=\"1\" />");
		if (DEFAULT_JS_FUNCTION.equals(this.jsFunction)) {
			str.append("<script>function ").append(DEFAULT_JS_FUNCTION).append("(pageNo){ $('#").append(this.inputId).append("').val(pageNo); $('#").append(this.inputId)
					.append("').parent('form').trigger('submit');}</script>");
		}
		return str.toString();
	}

	/** 输出HTML */
	public String getHtmlNav() {
		StringBuilder buf = new StringBuilder(650);
		int pageCount = this.getPageCount();
		int pageNo = this.getPageNo();

		buf.append("<ul class=\"pagination\">");

		// 上一页
		if (pageNo == 1 || pageCount == 0) {
			buf.append("<li class=\"disabled\"><span>&laquo;</span></li>");
		} else {
			buf.append("<li><a href=\"#\" onclick=\"gotoPage(").append(pageNo - 1).append(");return false;\">&laquo;</a></li>");
		}

		// 小于等于7页全部显示
		if (pageCount <= 7) {
			for (int i = 1; i <= pageCount; i++) {
				if (i == pageNo) {
					buf.append("<li class=\"active\"><span>").append(i).append("</span></li>");
				} else {
					buf.append("<li><a href=\"#\" onclick=\"gotoPage(").append(i).append(");return false;\">").append(i).append("</a></li>");
				}
			}
			// 当前页码靠前
		} else if (pageNo <= 4) {
			for (int i = 1; i <= pageCount; i++) {
				if (i == pageNo) {
					buf.append("<li class=\"active\"><span>").append(i).append("</a></li>");
				} else if (i <= pageNo + 1 || i == pageCount) {
					buf.append("<li><a href=\"#\" onclick=\"gotoPage(").append(i).append(");return false;\">").append(i).append("</a></li>");
				}
				// 后面的省略号
				if (i == pageNo + 1) {
					int target = (pageCount + pageNo + 1) / 2;
					buf.append("<li><a href=\"#\" title=\"").append(target).append("\" onclick=\"gotoPage(").append(target).append(");return false;\">&hellip;</li>");
				}
			}
			// 当前页靠后
		} else if (pageCount - pageNo <= 3) {
			for (int i = 1; i <= pageCount; i++) {
				if (i == pageNo) {
					buf.append("<li class=\"active\"><span>").append(i).append("</span></li>");
				} else if (i == 1 || i >= pageNo - 1) {
					buf.append("<li><a href=\"#\" onclick=\"return gotoPage(").append(i).append(");return false;\">").append(i).append("</a></li>");
				}
				// 前面的省略号
				if (i == 1) {
					int target = (pageNo + 1) / 2;
					buf.append("<li><a href=\"#\" title=\"").append(target).append("\" onclick=\"gotoPage(").append(target).append(");return false;\">&hellip;</li>");
				}
			}
			// 当前页处于中间段
		} else {
			for (int i = 1; i <= pageCount; i++) {
				if (i == pageNo) {
					buf.append("<li class=\"active\"><span>").append(i).append("</span></li>");
				} else if (i == 1 || i == pageCount || i == pageNo + 1 || i == pageNo - 1) {
					buf.append("<li><a href=\"#\" onclick=\"gotoPage(").append(i).append(");return false;\">").append(i).append("</a></li>");
				}
				// 前面的省略号
				if (i == 1) {
					int target = (pageNo + 1) / 2;
					buf.append("<li><a href=\"#\" title=\"").append(target).append("\" onclick=\"gotoPage(").append(target).append(");return false;\">&hellip;</li>");
				}
				// 后面的省略号
				if (i == pageNo + 1) {
					int target = (pageCount + pageNo + 1) / 2;
					buf.append("<li><a href=\"#\" title=\"").append(target).append("\" onclick=\"gotoPage(").append(target).append(");return false;\">&hellip;</li>");
				}
			}
		}

		// 下一页
		if (pageCount == 0 || pageNo == pageCount) {
			buf.append("<li class=\"disabled\"><span>&raquo;</span></li>");
		} else {
			buf.append("<li><a href=\"#\" onclick=\"gotoPage(").append(pageNo + 1).append(");return false;\">&raquo;</a></li>");
		}
		buf.append("</ul>");
		return buf.toString();
	}

}
