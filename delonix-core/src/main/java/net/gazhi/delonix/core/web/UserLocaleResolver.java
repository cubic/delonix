package net.gazhi.delonix.core.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.gazhi.delonix.core.entity.LoginUser;
import net.gazhi.delonix.core.thread.ThreadContext;

import org.springframework.web.servlet.i18n.AbstractLocaleResolver;

public class UserLocaleResolver extends AbstractLocaleResolver {

	@Override
	public Locale resolveLocale(HttpServletRequest request) {
		LoginUser user = ThreadContext.getLoginUser();
		if(user.getId()<=0){
			user.setLocale(request.getLocale().getLanguage());
		}
		return user.getLocale();
	}

	@Override
	public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
		throw new UnsupportedOperationException("Cannot change RBAC locale - use a different locale resolution strategy");
	}

}