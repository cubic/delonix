package net.gazhi.delonix.core.entity;

import java.util.Set;

/**
 * 单表结构的无限级树节点的抽象
 * 
 * @author Jeffrey Lin
 *
 */
public interface TreeNode {

	Long getId();

	void setId(Long id);

	String getName();

	int getShowOrder();

	void setShowOrder(int showOrder);

	String getFullId();

	void setFullId(String fullId);

	String getFullOrder();

	void setFullOrder(String fullOrder);

	TreeNode getParent();

	void setParent(TreeNode parent);

	Set<? extends TreeNode> getChildren();
}
