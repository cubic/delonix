package net.gazhi.delonix.core.form;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 树节点移动表单
 * 
 * @author Jeffrey Lin
 *
 */
public class TreeNodeMoveForm {

	private long id;

	private long targetId;

	@NotBlank
	private String moveType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTargetId() {
		return targetId;
	}

	public void setTargetId(long targetId) {
		this.targetId = targetId;
	}

	public String getMoveType() {
		return moveType;
	}

	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}

}
