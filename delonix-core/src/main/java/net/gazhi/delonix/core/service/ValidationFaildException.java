package net.gazhi.delonix.core.service;

/**
 * 在边保存边验证的情况下，如果出现验证失败，想要回滚，可以通过抛出 ValidationFaildException 达到目的
 * 
 * @author Jeffrey Lin
 *
 */
public class ValidationFaildException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ValidationFaildException(String msg) {
		super(msg);
	}

}
