package net.gazhi.delonix.core.thread;

/**
 * 对象封包器，在把对象加入到线程缓存之前，先把对象封包<br>
 * <br>
 * 这个设计的好处主要是：<br>
 * 利用泛型特性，对存取数据的方法进行抽象，提高代码的复用率<br>
 * 如果有新的数据要加入，只需定义对应的Wrapper类，并简单添加存取方法即可，扩展方便
 * 
 * @author Jeffrey Lin
 * 
 * @param <T>
 */
public abstract class ObjectWrapper<T> {

	private T obj;

	public ObjectWrapper<T> wrap(T obj) {
		this.obj = obj;
		return this;
	}

	T getObj() {
		return obj;
	}

}
