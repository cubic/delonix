package net.gazhi.delonix.core.service;
/**
 * 无权操作指定的数据
 * 
 * @author Jeffrey Lin
 *
 */
public class DataAccessDeniedException extends RuntimeException {
	private static final long serialVersionUID = 1L;
}
