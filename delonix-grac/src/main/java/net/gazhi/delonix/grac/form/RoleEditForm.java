package net.gazhi.delonix.grac.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class RoleEditForm {

	private Long id;

	@NotBlank
	@Length(min = 2, max = 80)
	private String nameZh;

	@NotBlank
	@Length(min = 2, max = 80)
	private String nameEn;

	@NotBlank
	@Length(min = 1, max = 80)
	private String showOrder;

	private Long[] actionIds;

	private boolean isDisabled;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameZh() {
		return nameZh;
	}

	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(String showOrder) {
		this.showOrder = showOrder;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public boolean getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public Long[] getActionIds() {
		return actionIds;
	}

	public void setActionIds(Long[] actionIds) {
		this.actionIds = actionIds;
	}

}
