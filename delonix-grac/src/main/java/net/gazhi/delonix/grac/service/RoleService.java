package net.gazhi.delonix.grac.service;

import java.util.List;

import net.gazhi.delonix.core.dao.PageInfo;
import net.gazhi.delonix.core.service.AbstractEntityService;
import net.gazhi.delonix.grac.entity.gru.Role;
import net.gazhi.delonix.grac.form.RoleEditForm;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;

@Service
@Transactional(readOnly = true)
public class RoleService extends AbstractEntityService<Role> {

	@Autowired
	private ActionService actionService;

	/**
	 * 列出所有角色
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Role> listAll() {
		return (List<Role>) super.createCriteria().addOrder(Order.asc("isDisabled")).addOrder(Order.asc("showOrder")).list();
	}

	/**
	 * 分页查找角色
	 * 
	 * @return
	 */
	public List<Role> search(String keyword, PageInfo pageInfo) {
		Criteria query = createCriteria("r");
		if (StringUtils.hasText(keyword)) {
			query.add(Restrictions.or(Restrictions.like("r.nameZh", "%" + keyword + "%"), Restrictions.like("r.nameEn", "%" + keyword + "%")));
		}
		return super.listPage(query, pageInfo, Order.asc("r.isDisabled"), Order.asc("r.showOrder"));
	}

	/**
	 * 保存角色(新增/修改)
	 * 
	 * @param form
	 * @param result
	 */
	@Transactional
	public void save(RoleEditForm form, BindingResult result) {
		Role role = null;
		if (form.getId() != null) {
			if (new Long(1).equals(form.getId())) {
				result.rejectValue("id", "SA-Locked", "Can NOT Modify Super Administrator!");
			}
			role = this.get(form.getId());
			if (role == null) {
				result.rejectValue("id", "NotFound", new String[] { String.valueOf(form.getId()) }, "Role NOT Found!");
				return;
			}
			if (super.otherExisted(form.getId(), "nameZh", form.getNameZh())) {
				result.rejectValue("nameZh", "Exists", new String[] { form.getNameZh() }, "Chiniese-Name already exists!");
			}
			if (super.otherExisted(form.getId(), "nameEn", form.getNameEn())) {
				result.rejectValue("nameEn", "Exists", new String[] { form.getNameEn() }, "English-Name already exists!");
			}
			if (!result.hasErrors()) {
				BeanUtils.copyProperties(form, role);
				dao.update(role);
			}
		} else {
			if (super.existed("nameZh", form.getNameZh())) {
				result.rejectValue("nameZh", "Exists", new String[] { form.getNameZh() }, "Chiniese-Name already exists!");
			}
			if (super.existed("nameEn", form.getNameEn())) {
				result.rejectValue("nameEn", "Exists", new String[] { form.getNameEn() }, "English-Name already exists!");
			}
			if (!result.hasErrors()) {
				role = new Role();
				BeanUtils.copyProperties(form, role);
				dao.save(role);
			}
		}
		role.getActions().clear();
		if (form.getActionIds() == null) {
			return;
		}
		for (Long actionId : form.getActionIds()) {
			role.getActions().add(this.actionService.get(actionId));
		}
	}

	/**
	 * 删除角色
	 * 
	 * @param id
	 * @param result
	 */
	@Transactional
	public void delete(long id, BindingResult result) {
		if (id == 1) {
			result.rejectValue("id", "SA-Locked", new Object[] { id }, "Can NOT delete Super Administrator!");
		}
		Role role = this.get(id);
		if (role == null) {
			result.rejectValue("id", "NotFound", new Object[] { id }, "Role NOT Found!");
			return;
		}
		dao.delete(role);
	}

}
