package net.gazhi.delonix.grac.web.controller.pub;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import net.gazhi.delonix.core.entity.LoginUser;
import net.gazhi.delonix.core.thread.ThreadContext;
import net.gazhi.delonix.core.web.AbstractAnnotationController;
import net.gazhi.delonix.grac.entity.gru.User;
import net.gazhi.delonix.grac.form.LoginForm;
import net.gazhi.delonix.grac.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class AuthenticationController extends AbstractAnnotationController {

	/**
	 * 加密，用于手动加密密码
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = null;
		try {
			System.out.print("请输入要加密的字符串：");
			s = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		encoder.encode(s);
		System.out.println("加密结果：" + encoder.encode(s));
	}

	private static final String LOGIN_RANDOM = "net.gazhi.delonix.LOGIN_RANDOM";

	@Autowired
	UserService userService;

	/**
	 * 登录表单
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		return new ModelAndView();
	}

	/**
	 * 处理登录请求
	 * 
	 * @param session
	 * @param loginName
	 * @param loginPsw
	 * @param randomCode
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView postLogin(@Valid @ModelAttribute("grac$login") LoginForm form, BindingResult result, HttpSession session) {
		if (!result.hasErrors()) {
			if (!form.getRandomCode().equalsIgnoreCase((String) session.getAttribute(LOGIN_RANDOM))) {
				result.rejectValue("randomCode", "Mismatched");
			} else {
				User user = userService.findByLoginName(form.getLoginName());
				BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
				if (user == null || !encoder.matches(form.getLoginPsw(), user.getLoginPsw())) {
					result.reject("Mismatched");
				} else if (user.isDisabled()) {
					result.reject("Disabled");
				} else {
					LoginUser loginUser = new LoginUser(user.getId(), user.getLoginName(), user.getNameEn(), user.getNameZh(), user.getLocale());
					LoginUser.setLoginUser(session, loginUser);
					ThreadContext.setLoginUser(loginUser);
				}
			}
		}
		session.removeAttribute(LOGIN_RANDOM);
		return new ModelAndView("loginResult");
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpSession session) {
		session.invalidate();
		return new ModelAndView();
	}

	/**
	 * 生成验证码图片
	 * 
	 * @param session
	 * @param response
	 */
	@RequestMapping("/loginRandom")
	public void loginRandom(HttpSession session, HttpServletResponse response) {
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setContentType("image/jpeg");
		// 在内存中创建图象
		int width = 80, height = 28;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		// 获取图形上下文
		Graphics g = image.getGraphics();
		// 生成随机类
		Random random = new Random();
		// 设定背景色
		g.setColor(getRandColor(200, 250));
		g.fillRect(0, 0, width, height);
		// 设定字体
		g.setFont(new Font("Times New Roman", Font.PLAIN, 26));
		// 画边框
		// g.setColor(new Color());
		// g.drawRect(0,0,width-1,height-1);
		// 随机产生150条干扰线，使图象中的认证码不易被其它程序探测到

		for (int i = 0; i < 150; i++) {
			g.setColor(getRandColor(100, 200));
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int xl = random.nextInt(12);
			int yl = random.nextInt(12);
			g.drawLine(x, y, x + xl, y + yl);
		}
		// 取随机产生的认证码(4位数字)
		// String rand = request.getParameter("rand");
		// rand = rand.substring(0,rand.indexOf("."));
		String sRand = "";
		for (int i = 0; i < 4; i++) {
			String rand = String.valueOf(random.nextInt(10));
			sRand += rand;
			// 将认证码显示到图象中
			g.setColor(new Color(20 + random.nextInt(110), 20 + random.nextInt(110), 20 + random.nextInt(110)));// 调用函数出来的颜色相同，可能是因为种子太接近，所以只能直接生成
			g.drawString(rand, 18 * i + 6, 24);
		}
		// 将认证码存入SESSION
		session.setAttribute(LOGIN_RANDOM, sRand);

		// 图象生效
		g.dispose();
		// 输出图象到页面
		try {
			ImageIO.write(image, "JPEG", response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// 给定范围获得随机颜色
	private Color getRandColor(int fc, int bc) {
		Random random = new Random();
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}

}
