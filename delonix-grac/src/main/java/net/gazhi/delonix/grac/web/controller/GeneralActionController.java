package net.gazhi.delonix.grac.web.controller;

import net.gazhi.delonix.core.web.AbstractAnnotationController;
import net.gazhi.delonix.grac.service.UserService;
import net.gazhi.delonix.grac.web.annonation.Handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
@Handler(zh = "常规操作", en = "GeneralActions")
public class GeneralActionController extends AbstractAnnotationController {

	@Autowired
	UserService userService;

	@RequestMapping("/switchLocale")
	@Handler(zh = "修改用户语言", en = "Switch User Locale")
	public ModelAndView switchLocale(String locale) {
		this.userService.switchLocale(locale);
		return null;
	}

}
