package net.gazhi.delonix.grac.web.annonation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/**
 * 在 package-info.java 对 Controller 进行排序
 * 
 * @author Jeffrey Lin
 *
 */
@Target(ElementType.PACKAGE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SortedControllers {
	Class<?>[] value();

}
