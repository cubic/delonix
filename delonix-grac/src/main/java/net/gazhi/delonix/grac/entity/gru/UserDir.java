package net.gazhi.delonix.grac.entity.gru;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import net.gazhi.delonix.core.entity.BilingualName;
import net.gazhi.delonix.core.entity.TreeNode;
import net.gazhi.delonix.grac.AC_TB;

/**
 * 系统用户目录
 * 
 * @author Jeffrey Lin
 * @since 1.0.0
 */
@Entity(name = AC_TB.ac_user_dir)
public class UserDir extends BilingualName implements TreeNode, Comparable<UserDir> {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "full_id")
	private String fullId;

	@Column(name = "name_zh")
	private String nameZh;

	@Column(name = "name_en")
	private String nameEn;

	@Column(name = "show_order")
	private int showOrder;

	@Column(name = "full_order")
	private String fullOrder;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", nullable = true)
	private UserDir parent;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = UserDir.class)
	@JoinColumn(name = "parent_id", updatable = false)
	private Set<UserDir> children = new TreeSet<UserDir>();

	public UserDir() {
	}

	public UserDir(UserDir parent, String nameZh, String nameEn, String fullId, int showOrder, String fullOrder) {
		this.parent = parent;
		this.nameZh = nameZh;
		this.nameEn = nameEn;
		this.fullId = fullId;
		this.showOrder = showOrder;
		this.fullOrder = fullOrder;
	}

	public UserDir(UserDir parent) {
		this.parent = parent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullId() {
		return fullId;
	}

	public void setFullId(String fullId) {
		this.fullId = fullId;
	}

	public String getNameZh() {
		return nameZh;
	}

	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public int getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(int showOrder) {
		this.showOrder = showOrder;
	}

	public String getFullOrder() {
		return fullOrder;
	}

	public void setFullOrder(String fullOrder) {
		this.fullOrder = fullOrder;
	}

	public UserDir getParent() {
		return parent;
	}

	@Override
	public void setParent(TreeNode parent) {
		this.parent = (UserDir) parent;
	}

	public Set<UserDir> getChildren() {
		return children;
	}

	public String getFullName() {
		return (this.getParent() == null ? "" : this.getParent().getFullName()) + "/" + this.getName();
	}

	@Override
	public int compareTo(UserDir target) {
		return this.fullOrder.compareTo(target.getFullOrder());
	}

}
