package net.gazhi.delonix.grac.entity.gru;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import net.gazhi.delonix.core.entity.BilingualName;
import net.gazhi.delonix.grac.AC_TB;
import net.gazhi.delonix.grac.entity.act.Action;

import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;

/**
 * 角色
 * 
 * @author Jeffrey Lin
 */
@Entity(name = AC_TB.ac_role)
public class Role extends BilingualName {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "name_en")
	private String nameEn;

	@Column(name = "name_zh")
	private String nameZh;

	@Column(name = "show_order")
	private String showOrder;

	@Column(name = "is_disabled")
	private boolean isDisabled;

	/**
	 * 授予角色的 action 列表
	 */
	@ManyToMany(fetch = FetchType.LAZY, targetEntity = Action.class)
	@JoinTable(name = AC_TB.ac_role_action, joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "action_id"))
	private Set<Action> actions;

	/**
	 * 授予角色的用户列表
	 */
	@ManyToMany(fetch = FetchType.LAZY, targetEntity = User.class)
	@JoinTable(name = AC_TB.ac_user_role, joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	@Sort(type = SortType.NATURAL)
	private Set<User> users;

	public Set<User> getUsers() {
		return users;
	}

	public Set<Action> getActions() {
		if (actions == null) {
			actions = new TreeSet<Action>();
		}
		return actions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameZh() {
		return nameZh;
	}

	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(String showOrder) {
		this.showOrder = showOrder;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public boolean getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

}
