package net.gazhi.delonix.grac.web.controller;

import java.util.List;

import javax.validation.Valid;

import net.gazhi.delonix.core.form.TreeNodeMoveForm;
import net.gazhi.delonix.grac.entity.gru.UserDir;
import net.gazhi.delonix.grac.form.UserDirEditForm;
import net.gazhi.delonix.grac.service.UserDirService;
import net.gazhi.delonix.grac.web.annonation.Handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/grac/userDir")
@Handler(zh = "用户目录管理", en = "User Directories")
public class UserDirController extends AbstractGracController {

	@Autowired
	private UserDirService userDirService;

	@Handler(zh = "首页面", en = "Home Page", redirectable = true)
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home() {
		List<UserDir> dirs = this.userDirService.listAll();
		return new ModelAndView().addObject("dirs", dirs);
	}

	@Handler(zh = "保存目录", en = "Save Directory")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute("grac$user-dir$edit") UserDirEditForm form, BindingResult result) {
		if (!result.hasErrors()) {
			this.userDirService.save(form, result);
		}
		return new ModelAndView();
	}

	@Handler(zh = "移动目录", en = "Move Directory")
	@RequestMapping(value = "/move", method = RequestMethod.GET)
	public ModelAndView move(@Valid @ModelAttribute("grac$user-dir$move") TreeNodeMoveForm form, BindingResult result) {
		this.userDirService.move(form, result);
		return new ModelAndView();
	}

	@Handler(zh = "删除目录", en = "Delete Directory")
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@ModelAttribute("grac$user-dir$delete") UserDir dir, BindingResult result) {
		this.userDirService.delete(dir.getId(), result);
		return new ModelAndView();
	}

}
