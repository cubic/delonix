package net.gazhi.delonix.grac.web.annonation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 作用于 Package, Controller 和 Method 的注解<br>
 * 可以配置：中/英文名字、访问许可类型、是否允许登录后自动跳转、是否记录访问日志
 * 
 * @author Jeffrey Lin
 * 
 */
@Target({ ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Handler {

	/** 中文名字 */
	String zh();

	/** 英文名字 */
	String en();

	/** 是否允许登录后自动跳转，默认为 false */
	boolean redirectable() default false;

	/** 是否记录访问日志，默认为 true */
	boolean logging() default true;

}
