package net.gazhi.delonix.grac.service;

import java.util.List;

import net.gazhi.delonix.core.service.AbstractEntityService;
import net.gazhi.delonix.grac.entity.act.Action;

import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class ActionService extends AbstractEntityService<Action> {

	public static long generateActionId(String fullMethodName) {
		return Math.abs(fullMethodName.hashCode());
	}

	/**
	 * 列出所有 Action
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Action> list() {
		return (List<Action>) createCriteria("act").createAlias("act.controller", "ctr").createAlias("ctr.module", "mdl").addOrder(Order.asc("mdl.id")).addOrder(Order.asc("ctr.id"))
				.addOrder(Order.asc("act.showOrder")).list();
	}

}
