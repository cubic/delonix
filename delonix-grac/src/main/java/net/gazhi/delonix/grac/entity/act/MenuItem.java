package net.gazhi.delonix.grac.entity.act;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import net.gazhi.delonix.core.entity.BilingualName;
import net.gazhi.delonix.grac.AC_TB;

/**
 * 菜单项<br>
 * 系统可以定制多套菜单，menuUsage 相同的菜单项属于同一菜单
 * 
 * @author Jeffrey Lin
 */
@Entity(name = AC_TB.ac_menu_item)
public class MenuItem extends BilingualName implements Comparable<MenuItem> {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "menu_usage")
	private String menuUsage;

	@Column(name = "name_zh")
	private String nameZh;

	@Column(name = "name_en")
	private String nameEn;

	@Column(name = "href")
	private String href;

	@Column(name = "target")
	private String target;

	@Column(name = "onclick")
	private String onclick;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private MenuItem parent;

	@ManyToOne
	@JoinColumn(name = "action_id")
	private Action action;

	@Transient
	private Set<MenuItem> childItems = new LinkedHashSet<MenuItem>();

	public Set<MenuItem> getChildItems() {
		return childItems;
	}

	public MenuItem() {
	}

	public MenuItem(String menuUsage, MenuItem parent, String nameEn, String nameZh, String href, String target, String onclick, Action action) {
		this.menuUsage = menuUsage;
		this.parent = parent;
		this.nameEn = nameEn;
		this.nameZh = nameZh;
		this.href = href;
		this.target = target;
		this.onclick = onclick;
		this.action = action;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMenuUsage() {
		return menuUsage;
	}

	public void setMenuUsage(String menuUsage) {
		this.menuUsage = menuUsage;
	}

	public String getNameZh() {
		return nameZh;
	}

	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getOnclick() {
		return onclick;
	}

	public void setOnclick(String onclick) {
		this.onclick = onclick;
	}

	public MenuItem getParent() {
		return parent;
	}

	public void setParent(MenuItem parent) {
		this.parent = parent;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	@Override
	public int compareTo(MenuItem target) {
		return this.getId().compareTo(target.getId());
	}

}
