package net.gazhi.delonix.grac.web.controller.pub;

import javax.servlet.http.HttpServletResponse;

import net.gazhi.delonix.core.web.AbstractAnnotationController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class ErrorPageController extends AbstractAnnotationController {

	public static final String PAGE_NOT_FOUND = "/error/not-found";
	public static final String PAGE_NO_LOGIN = "/error/no-login";
	public static final String PAGE_ACTION_ACCESS_DEINED = "/error/action-access-denied";
	public static final String PAGE_DATA_ACCESS_DEINED = "/error/data-access-denied";
	public static final String PAGE_INTERNAL_SERVER_ERROR = "/error/internal-server-error";

	@ModelAttribute("isAjax")
	public boolean isAjax(@RequestHeader(value = "X-Requested-With", required = false) String xRequestedWith) {
		return "XMLHttpRequest".equals(xRequestedWith);
	}

	/**
	 * 客户端 404 错误
	 * 
	 * @return
	 */
	@RequestMapping(value = PAGE_NOT_FOUND)
	public ModelAndView notFound(HttpServletResponse response) {
		response.setStatus(404);
		return new ModelAndView();
	}

	/**
	 * 未登录提示页面
	 * 
	 * @return
	 */
	@RequestMapping(value = PAGE_NO_LOGIN)
	public ModelAndView noLogin(HttpServletResponse response) {
		response.setStatus(401);
		return new ModelAndView();
	}

	/**
	 * 无权访问 Action
	 * 
	 * @return
	 */
	@RequestMapping(value = PAGE_ACTION_ACCESS_DEINED)
	public ModelAndView actionAccessDenied(HttpServletResponse response) {
		response.setStatus(403);
		return new ModelAndView();
	}

	/**
	 * 无权访问数据
	 * 
	 * @return
	 */
	@RequestMapping(value = PAGE_DATA_ACCESS_DEINED)
	public ModelAndView dataAccessDenied(HttpServletResponse response) {
		response.setStatus(403);
		return new ModelAndView();
	}

	/**
	 * 服务器 500 错误
	 * 
	 * @return
	 */
	@RequestMapping(value = PAGE_INTERNAL_SERVER_ERROR)
	public ModelAndView serverError(HttpServletResponse response) {
		response.setStatus(500);
		return new ModelAndView();
	}

}
