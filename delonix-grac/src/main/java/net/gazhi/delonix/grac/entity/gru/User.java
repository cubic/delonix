package net.gazhi.delonix.grac.entity.gru;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import net.gazhi.delonix.core.entity.BilingualName;
import net.gazhi.delonix.grac.AC_TB;

import org.springframework.util.StringUtils;

/**
 * 用户
 * 
 * @author Jeffrey Lin
 */
@Entity(name = AC_TB.ac_user)
public class User extends BilingualName implements Comparable<User> {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "name_en")
	private String nameEn;

	@Column(name = "name_zh")
	private String nameZh;

	@Column(name = "locale")
	private String locale;

	@Column(name = "show_order")
	private String showOrder;

	@Column(name = "login_name")
	private String loginName;

	@Column(name = "login_psw")
	private String loginPsw;

	@Column(name = "email")
	private String email;

	@Column(name = "is_disabled")
	private boolean isDisabled;

	@Column(name = "remark")
	private String remark;

	@Column(name = "create_user_id")
	private Long createUserId;

	@Column(name = "update_user_id")
	private Long updateUserId;

	@Column(name = "create_time")
	private Timestamp createTime;

	@Column(name = "update_time")
	private Timestamp updateTime;

	/**
	 * 用户目录
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dir_id", nullable = false)
	private UserDir userDir;

	/**
	 * 授予用户的角色列表
	 */
	@ManyToMany(fetch = FetchType.LAZY, targetEntity = Role.class)
	@JoinTable(name = AC_TB.ac_user_role, joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new LinkedHashSet<Role>();

	/**
	 * @return 角色列表
	 */
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * @return 所有角色名字
	 */
	public String getRoleNames() {
		List<String> list = new ArrayList<String>();
		for (Role r : this.getRoles()) {
			list.add(r.getName());
		}
		return StringUtils.collectionToDelimitedString(list, ", ");
	}

	public UserDir getUserDir() {
		return userDir;
	}

	public void setUserDir(UserDir userDir) {
		this.userDir = userDir;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getNameZh() {
		return nameZh;
	}

	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(String showOrder) {
		this.showOrder = showOrder;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPsw() {
		return loginPsw;
	}

	public void setLoginPsw(String loginPsw) {
		this.loginPsw = loginPsw;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public boolean getIsDisabled() {
		return isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public int compareTo(User target) {
		int i = (this.getIsDisabled() == target.getIsDisabled()) ? 0 : (this.getIsDisabled() ? 1 : -1);
		if (i == 0) {
			i = this.getShowOrder().compareTo(target.getShowOrder());
		}
		if (i == 0) {
			i = this.getId().compareTo(target.getId());
		}
		return i;
	}

}