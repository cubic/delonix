package net.gazhi.delonix.grac.entity.act;

import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import net.gazhi.delonix.core.entity.BilingualName;
import net.gazhi.delonix.grac.AC_TB;

/**
 * Controller 所属模块(package)
 * 
 * @author Jeffrey Lin
 */
@Entity(name = AC_TB.ac_module)
public class Module extends BilingualName implements Comparable<Module> {

	@Id
	private Long id;

	@Column(name = "name_zh")
	private String nameZh;

	@Column(name = "name_en")
	private String nameEn;

	@Column(name = "pkg_name")
	private String pkgName;

	/**
	 * 非持久化集合，用于输出计算
	 */
	@Transient
	private TreeSet<Controller> controllers = new TreeSet<Controller>();

	public Module() {
	}

	public Module(long id, String nameZh, String nameEn, String pkgName) {
		this.id = id;
		this.nameZh = nameZh;
		this.nameEn = nameEn;
		this.pkgName = pkgName;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String getNameZh() {
		return nameZh;
	}

	@Override
	public String getNameEn() {
		return nameEn;
	}

	public String getPkgName() {
		return pkgName;
	}

	/**
	 * 非持久化集合，用于输出计算
	 * 
	 * @return
	 */
	public TreeSet<Controller> getControllers() {
		return controllers;
	}

	@Override
	public int compareTo(Module target) {
		return this.id.compareTo(target.id);
	}

}
