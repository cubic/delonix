package net.gazhi.delonix.grac.form;

public class UserSearchForm {
	
	private int pageNo=1;
	private String keyword;
	private Long dirId;

	/** 是否显示下级目录的用户 */
	private boolean listSubdir;
	/** 点击目录时，是否立即执行搜索 */
	private boolean immediately;
	
	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long getDirId() {
		return dirId;
	}

	public void setDirId(Long dirId) {
		this.dirId = dirId;
	}

	public boolean isListSubdir() {
		// 首次请求，默认为 true
		if (keyword == null) {
			return true;
		}
		return listSubdir;
	}

	public void setListSubdir(boolean listSubdir) {
		this.listSubdir = listSubdir;
	}

	public boolean isImmediately() {
		// 首次请求，默认为 true
		if (keyword == null) {
			return true;
		}
		return immediately;
	}

	public void setImmediately(boolean immediately) {
		this.immediately = immediately;
	}

}
