@net.gazhi.delonix.grac.web.annonation.SortedControllers({
	GeneralActionController.class,
	UserController.class,
	UserDirController.class,
	RoleController.class
	})
@net.gazhi.delonix.grac.web.annonation.Handler(zh = "GRAC 模块", en = "GRAC Module")
package net.gazhi.delonix.grac.web.controller;