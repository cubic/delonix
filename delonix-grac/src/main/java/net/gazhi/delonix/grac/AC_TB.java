package net.gazhi.delonix.grac;

/**
 * 定义数据库中的表
 * 
 * @author Jeffrey Lin
 * 
 */
public interface AC_TB {

	/**
	 * 用户目录
	 */
	static final String ac_user_dir="ac_user_dir";
	/** 用户 */
	static final String ac_user = "ac_user";
	/** 分组 */
	static final String ac_group = "ac_group";
	/** 角色 */
	static final String ac_role = "ac_role";
	/** 用户分组 */
	static final String ac_user_group = "ac_user_group";
	/** 用户角色 */
	static final String ac_user_role = "ac_user_role";

	/** 请求处理控制器所在的包模块 */
	static final String ac_module = "ac_module";
	/** 请求处理控制器 */
	static final String ac_controller = "ac_controller";
	/** 请求处理方法 */
	static final String ac_action = "ac_action";
	/** 菜单项定义 */
	static final String ac_menu_item = "ac_menu_item";

	/** 资源实体 */
	static final String ac_res = "ac_res";
	/** 资源实体类型 */
	static final String ac_res_type = "ac_res_type";
	/** 下属资源实体的可选类型 */
	static final String ac_res_type_child = "ac_res_type_child"; 
	/** 资源操作 */
	static final String ac_op = "ac_op";
	/** 资源类型关联操作 */
	static final String ac_res_type_op = "ac_res_type_op";

	/** [角色-Action] 授权 */
	static final String ac_role_action = "ac_role_action";
	/** [分组-操作-资源] 授权 */
	static final String ac_group_op_res = "ac_group_op_res";
	
	/** [用户-操作-资源] 授权视图 */
	static final String _ac_user_op_res="_ac_user_op_res";
	/** [用户-操作-资源] 授权内存表 */
	static final String ac_user_op_res="ac_user_op_res";
	
}
