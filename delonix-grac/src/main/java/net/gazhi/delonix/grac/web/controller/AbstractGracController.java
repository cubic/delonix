package net.gazhi.delonix.grac.web.controller;

import net.gazhi.delonix.core.service.DataAccessDeniedException;
import net.gazhi.delonix.core.web.AbstractAnnotationController;
import net.gazhi.delonix.grac.web.controller.pub.ErrorPageController;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

public class AbstractGracController extends AbstractAnnotationController {

	/**
	 * 处理非法请求异常
	 * 
	 * @param e
	 * @return
	 */
	@ExceptionHandler(DataAccessDeniedException.class)
	public ModelAndView handleInvalidAccessException(DataAccessDeniedException e) {
		logger.warn(e.getMessage(), e);
		return new ModelAndView("forward:" + ErrorPageController.PAGE_DATA_ACCESS_DEINED);
	}
}
