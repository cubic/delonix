package net.gazhi.delonix.grac.thread;

import java.util.List;
import net.gazhi.delonix.core.thread.ObjectWrapper;
import net.gazhi.delonix.core.thread.ThreadContext;
import net.gazhi.delonix.grac.entity.act.Action;

public abstract class GracThreadContext extends ThreadContext {

	public static void setAuthorizedActions(List<Action> actions) {
		set(new AuthorizedActionsWrapper().wrap(actions));
	}

	public static List<Action> getAuthorizedActions() {
		return get(AuthorizedActionsWrapper.class);
	}

	private static class AuthorizedActionsWrapper extends ObjectWrapper<List<Action>> {
	}
}
