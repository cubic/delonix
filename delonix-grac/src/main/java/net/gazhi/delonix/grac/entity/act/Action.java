package net.gazhi.delonix.grac.entity.act;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import net.gazhi.delonix.core.entity.BilingualName;
import net.gazhi.delonix.grac.AC_TB;
import net.gazhi.delonix.grac.entity.gru.Role;

/**
 * Spring MVC 的请求处理Action方法
 * 
 * @author Jeffrey Lin
 */
@Entity(name = AC_TB.ac_action)
public class Action extends BilingualName implements Comparable<Action> {

	@Id
	private Long id;

	@Column(name = "name_zh")
	private String nameZh;

	@Column(name = "name_en")
	private String nameEn;

	@Column(name = "method_name")
	private String methodName;

	@Column(name = "show_order")
	private int showOrder;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "controller_id", nullable = false)
	private Controller controller;

	@ManyToMany(fetch = FetchType.LAZY, targetEntity = Role.class)
	@JoinTable(name = AC_TB.ac_role_action, joinColumns = @JoinColumn(name = "action_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new LinkedHashSet<Role>();

	public Action() {
	}

	public Action(Long id, String nameZh, String nameEn, String methodName, int showOrder, Controller controller) {
		this.id = id;
		this.nameZh = nameZh;
		this.nameEn = nameEn;
		this.methodName = methodName;
		this.showOrder = showOrder;
		this.controller = controller;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameZh() {
		return nameZh;
	}

	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public int getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(int showOrder) {
		this.showOrder = showOrder;
	}

	public Controller getController() {
		return controller;
	}

	public void setController(Controller controller) {
		this.controller = controller;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	@Override
	public int compareTo(Action target) {
		int i = this.getController().compareTo(target.getController());
		if (i == 0) {
			i = this.getShowOrder() - target.getShowOrder();
		}
		return i;
	}

}
