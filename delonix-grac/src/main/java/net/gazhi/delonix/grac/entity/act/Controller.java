package net.gazhi.delonix.grac.entity.act;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import net.gazhi.delonix.core.entity.BilingualName;
import net.gazhi.delonix.grac.AC_TB;

/**
 * Spring MVC 的请求处理控制器对应的持久化实体
 * 
 * @author Jeffrey Lin
 */
@Entity(name = AC_TB.ac_controller)
public class Controller extends BilingualName implements Comparable<Controller> {

	@Id
	private Long id;

	@Column(name = "name_zh")
	private String nameZh;

	@Column(name = "name_en")
	private String nameEn;

	@Column(name = "class_name")
	private String className;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "module_id", nullable = false)
	private Module module;

	/**
	 * 非持久化集合，用于输出计算
	 */
	@Transient
	private TreeSet<Action> actions = new TreeSet<Action>();

	public Controller() {
	}

	public Controller(Long id, String nameZh, String nameEn, String className, Module module) {
		this.id = id;
		this.nameZh = nameZh;
		this.nameEn = nameEn;
		this.className = className;
		this.module = module;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameZh() {
		return nameZh;
	}

	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	/**
	 * 非持久化集合，用于输出计算
	 */
	public Set<Action> getActions() {
		return actions;
	}

	@Override
	public int compareTo(Controller target) {
		int i = this.getModule().compareTo(target.getModule());
		if (i == 0) {
			i = this.getId().compareTo(target.getId());
		}
		return i;
	}

}
