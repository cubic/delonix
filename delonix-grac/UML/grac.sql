SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS ac_menu_item;
DROP TABLE IF EXISTS ac_role_action;
DROP TABLE IF EXISTS ac_action;
DROP TABLE IF EXISTS ac_controller;
DROP TABLE IF EXISTS ac_module;
DROP TABLE IF EXISTS ac_user_role;
DROP TABLE IF EXISTS ac_role;
DROP TABLE IF EXISTS ac_user;
DROP TABLE IF EXISTS ac_user_dir;




/* Create Tables */

CREATE TABLE ac_action
(
	id bigint NOT NULL,
	controller_id bigint NOT NULL,
	name_zh varchar(80) NOT NULL,
	name_en varchar(80) NOT NULL,
	method_name varchar(80) NOT NULL,
	show_order int NOT NULL,
	PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE ac_controller
(
	id bigint NOT NULL AUTO_INCREMENT,
	module_id bigint NOT NULL,
	name_zh varchar(80),
	name_en varchar(80),
	class_name varchar(200),
	show_order int,
	PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE ac_menu_item
(
	id bigint NOT NULL AUTO_INCREMENT,
	action_id bigint,
	parent_id bigint,
	menu_usage varchar(64) NOT NULL COMMENT '用途相同的菜单项组成一个菜单',
	name_zh varchar(80) NOT NULL,
	name_en varchar(80) NOT NULL,
	href varchar(80),
	target varchar(80),
	onclick varchar(80),
	PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '菜单定义';


CREATE TABLE ac_module
(
	id bigint NOT NULL AUTO_INCREMENT,
	name_zh varchar(80) NOT NULL,
	name_en varchar(80) NOT NULL,
	pkg_name varchar(200) NOT NULL,
	PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE ac_role
(
	id bigint NOT NULL AUTO_INCREMENT,
	name_zh varchar(80) NOT NULL,
	name_en varchar(80) NOT NULL,
	show_order varchar(80) NOT NULL,
	is_disabled int NOT NULL,
	PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE ac_role_action
(
	role_id bigint NOT NULL,
	action_id bigint NOT NULL,
	PRIMARY KEY (role_id, action_id)
) ENGINE = InnoDB;


CREATE TABLE ac_user
(
	id bigint NOT NULL AUTO_INCREMENT,
	dir_id bigint NOT NULL,
	name_zh varchar(80),
	name_en varchar(80),
	locale char(2),
	show_order varchar(80),
	login_name varchar(40),
	login_psw char(80),
	email varchar(100),
	is_disabled int,
	remark varchar(128),
	create_user_id bigint,
	update_user_id bigint,
	create_time datetime,
	update_time datetime,
	PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE ac_user_dir
(
	id bigint NOT NULL AUTO_INCREMENT,
	parent_id bigint,
	name_zh varchar(100) NOT NULL,
	name_en varchar(100) NOT NULL,
	show_order int NOT NULL,
	full_id varchar(200),
	full_order varchar(200),
	PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE ac_user_role
(
	user_id bigint NOT NULL,
	role_id bigint NOT NULL,
	PRIMARY KEY (user_id, role_id)
) ENGINE = InnoDB;



